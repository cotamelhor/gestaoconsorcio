﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestaoConsorcio.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (User.IsInRole("BackOfficeDoc") || User.IsInRole("BackOfficeLig"))
            {
                return RedirectToAction("Agendamento", "Agendamento");
            }
            else if (User.IsInRole("Caixa"))
            {
                return RedirectToAction("Index", "Campanhas");
            }
            else { 
            return View("Index");
            }
        }

        public ActionResult Agendamento()
        {
            return View("Contact");
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}