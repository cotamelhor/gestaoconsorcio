﻿using GestaoConsorcio.Models;
using GestaoConsorcio.Models.Repositorio;
using GestaoConsorcio.Models.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using GestaoConsorcio.Models.Banco;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace GestaoConsorcio.Controllers
{
    [Authorize]
    public class ContratoController : Controller
    {
        ContratoBD c = new ContratoBD();

        public ActionResult Simulacao()
        {
            return View();
        }

        [HttpGet]
        public ActionResult SimulacaoCompleta()
        {
            ViewBag.DataAtual = DateTime.Now.Day.ToString("D2") + "/" + DateTime.Now.Month + "/" + DateTime.Now.Year;
            ViewBag.HoraAtual = DateTime.Now.Hour + ":" + (Int32.Parse(DateTime.Now.Minute.ToString()) < 10 ? "0" + DateTime.Now.Minute.ToString() : DateTime.Now.Minute.ToString());
            return View();
        }

        [HttpGet]
        public ActionResult SolicitarCompleta()
        {
            ViewBag.DataAtual = DateTime.Now.Day.ToString("D2") + "/" + DateTime.Now.Month + "/" + DateTime.Now.Year;
            ViewBag.HoraAtual = DateTime.Now.Hour + ":" + (Int32.Parse(DateTime.Now.Minute.ToString()) < 10 ? "0" + DateTime.Now.Minute.ToString() : DateTime.Now.Minute.ToString());
            return View();
        }

       
        [HttpPost]
        public ActionResult Simulacao(SimulacaoVM model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            model.Consultor = Int32.Parse(User.Identity.GetUserId());
            var retorno = c.Simulacao(model);

            if (retorno == "1")
            {
                ViewBag.Visivel = "";
                ViewBag.TipoAlert = "alert-success";
                ViewBag.Mensagem = "Solicitação salva com sucesso!";
            }
            if (retorno == "-1")
            {
                ViewBag.Visivel = "";
                ViewBag.TipoAlert = "alert-danger";
                ViewBag.Mensagem = "Problemas ao solicitar o contrato, por favor tente novamente!";
            }
            if (retorno == "0")
            {
                ViewBag.Visivel = "";
                ViewBag.TipoAlert = "alert-info";
                ViewBag.Mensagem = "Número de protocolo já existe!";
            }

            return View();
        }

        public ActionResult Solicitar()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Solicitar(DadosAgen model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            model.consultor = int.Parse(User.Identity.GetUserId());
            var retorno = c.Solicitacao(model);

            if (retorno == "1")
            {
                ViewBag.Visivel = "";
                ViewBag.TipoAlert = "alert-success";
                ViewBag.Mensagem = "Solicitação salva com sucesso!";
            }
            if (retorno == "-1")
            {
                ViewBag.Visivel = "";
                ViewBag.TipoAlert = "alert-danger";
                ViewBag.Mensagem = "Problemas ao solicitar o contrato, por favor tente novamente!";
            }
            if (retorno == "0")
            {
                ViewBag.Visivel = "";
                ViewBag.TipoAlert = "alert-info";
                ViewBag.Mensagem = "Número de protocolo já existe!";
            }

            return View("Solicitar",model);
        }

        public ActionResult SalvarContrato()
        {
            ComboBD cc = new ComboBD();

            ConsultorVM model = null;
            var consultores = cc.ComboConsultorBD();
            List<ConsultorVM> listconsul = new List<ConsultorVM>();

            foreach (ConsultorVM consul in consultores)
            {
                model = new ConsultorVM();
                model.consultorid = consul.consultorid;
                model.nome = consul.nome;
                model.sobrenome = consul.sobrenome;

                listconsul.Add(model);
            }
            SalvarContratoVM cvm = new SalvarContratoVM();
            cvm.consultores = listconsul;

            return View("SalvarContrato", cvm);
        }
        [HttpPost]
        public ActionResult SalvarContrato(SalvarContratoVM cont)
        {
            ComboBD cc = new ComboBD();

            ConsultorVM model = null;
            var consultores = cc.ComboConsultorBD();
            List<ConsultorVM> listconsul = new List<ConsultorVM>();

            foreach (ConsultorVM consul in consultores)
            {
                model = new ConsultorVM();
                model.consultorid = consul.consultorid;
                model.nome = consul.nome;
                model.sobrenome = consul.sobrenome;

                listconsul.Add(model);
            }
            cont.consultores = listconsul;

            if (!ModelState.IsValid)
            {
                
                ViewBag.Visivel = "hidden";
                return View(cont);
            }
           
            if (!User.IsInRole("Administrador"))
            {
                cont.ConsultorID = User.Identity.GetUserId().ToString();
            }

            cont.ConsultorID = Request.Form["ComboConsultor"];
            var retorno = c.ExisteContrato(cont);

            if (retorno == "1")
            {
                ViewBag.Visivel = "";
                ViewBag.TipoAlert = "alert-success";
                ViewBag.Mensagem = "Contrato salvo com sucesso!";
            }
            if (retorno == "0")
            {
                ViewBag.Visivel = "";
                ViewBag.TipoAlert = "alert-danger";
                ViewBag.Mensagem = "Problemas ao salvar o contrato, por favor tente novamente!";
            }
            if (retorno == "-1")
            {
                ViewBag.Visivel = "";
                ViewBag.TipoAlert = "alert-info";
                ViewBag.Mensagem = "Contrato já existe!";
            }
             
            return View(cont);
        }

        public ActionResult BuscarContrato()
        {
            ContratoVM model = new ContratoVM();
            model.fg_Hidden = "hidden";
            model.fg_HiddenVenda = "hidden";
            model.fg_HiddenSaldoAcumulado = "hidden";
            model.fg_HiddenErro = "hidden";
            ViewBag.Table = "hidden";
            model.NumeroContrato = "";
            return View("BuscarContrato",model);
        }

        public ActionResult Relatorio()
        {

            return View("Relatorio", c.BuscarRelatorioContrato());

        }

        public string FiltrarRelatorio(string nrocontr,
                                                    string situacao,
                                                    string datainicial,
                                                    string datafinal,
                                                    int pag)
        {
            if (string.IsNullOrEmpty(nrocontr)) nrocontr = "0";

            return JsonConvert.SerializeObject(c.BuscarRelatorioContrato(Convert.ToInt32(nrocontr),
                                                    situacao,
                                                    datainicial,
                                                    datafinal,
                                                    pag), Formatting.Indented, new JsonSerializerSettings() { DateFormatString = "yyyy-MM-ddThh:mm:ssZ" });


        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult BuscarContrato(ContratoVM model)
        {
            model.CheckBoxAlocado = Request.Form["CheckBoxAlocado"];

            var retorno = c.BuscarContrato(model.NumeroContrato, model.nome, model.CheckBoxAlocado, model.InicioFiltro, model.FimFiltro);
            ViewBag.Contrato = model.NumeroContrato;
            ViewBag.Nome = model.nome;
            ViewBag.CheckBoxAlocado = model.CheckBoxAlocado;
            ViewBag.Quantidade = retorno.Count;
            ViewBag.Retorno = retorno.Take(5).ToList();

            return View(model);
           
        }

        public JsonResult AlocadoContrato(int id, bool alocado)
        {
            var al = c.AlocadoContratoBD(id, alocado);

            return Json(al);
        }
        public JsonResult DetalheContrato(int id)
        {
            var retorno = c.DetalheContrato(id);
            ContratoVM model = new ContratoVM();
            
            foreach (ContratoVM cm in retorno)
            {

                if (cm.situacao == null || cm.situacao == "" || cm.nome == null)
                {
                    model.fg_Erro = "erro";
                }
                else
                {
                    ViewBag.Fg_Erro = "hidden";
                    string dtadesao = (cm.DtAdesao != null && cm.DtAdesao != "" ? (DateTime.Parse(cm.DtAdesao).Day < 10 ? "0" + DateTime.Parse(cm.DtAdesao).Day.ToString() : DateTime.Parse(cm.DtAdesao).Day.ToString()) + "/" + (DateTime.Parse(cm.DtAdesao).Month < 10 ? "0" + DateTime.Parse(cm.DtAdesao).Month.ToString() : DateTime.Parse(cm.DtAdesao).Month.ToString()) + DateTime.Parse(cm.DtAdesao).Year : "");
                    string dtalocacao = (cm.DtAlocacao != null && cm.DtAlocacao != "" ? (DateTime.Parse(cm.DtAlocacao).Day < 10 ? "0" + DateTime.Parse(cm.DtAlocacao).Day.ToString() : DateTime.Parse(cm.DtAlocacao).Day.ToString()) + "/" + (DateTime.Parse(cm.DtAlocacao).Month < 10 ? "0" + DateTime.Parse(cm.DtAlocacao).Month.ToString() : DateTime.Parse(cm.DtAlocacao).Month.ToString()) + DateTime.Parse(cm.DtAlocacao).Year : "");
                    string dtassembleia = (cm.DtAssembleia != null && cm.DtAssembleia != "" ? (DateTime.Parse(cm.DtAssembleia).Day < 10 ? "0" + DateTime.Parse(cm.DtAssembleia).Day.ToString() : DateTime.Parse(cm.DtAssembleia).Day.ToString()) + "/" + (DateTime.Parse(cm.DtAssembleia).Month < 10 ? "0" + DateTime.Parse(cm.DtAssembleia).Month.ToString() : DateTime.Parse(cm.DtAssembleia).Month.ToString()) + DateTime.Parse(cm.DtAssembleia).Year : "");
                    string dtcadastro = (cm.DtCadastro != null && cm.DtCadastro != "" ? (DateTime.Parse(cm.DtCadastro).Day < 10 ? "0" + DateTime.Parse(cm.DtCadastro).Day.ToString() : DateTime.Parse(cm.DtCadastro).Day.ToString()) + "/" + (DateTime.Parse(cm.DtCadastro).Month < 10 ? "0" + DateTime.Parse(cm.DtCadastro).Month.ToString() : DateTime.Parse(cm.DtCadastro).Month.ToString()) + DateTime.Parse(cm.DtCadastro).Year : "");
                    string dtnascimento = (cm.DtNascimento != null && cm.DtNascimento != "" ? (DateTime.Parse(cm.DtNascimento).Day < 10 ? "0" + DateTime.Parse(cm.DtNascimento).Day.ToString() : DateTime.Parse(cm.DtNascimento).Day.ToString()) + "/" + (DateTime.Parse(cm.DtNascimento).Month < 10 ? "0" + DateTime.Parse(cm.DtNascimento).Month.ToString() : DateTime.Parse(cm.DtNascimento).Month.ToString()) + DateTime.Parse(cm.DtNascimento).Year : "");
                    string dtassembleiatual = (cm.DtAssembleiaAtual != null && cm.DtAssembleiaAtual != "" ? (DateTime.Parse(cm.DtAssembleiaAtual).Day < 10 ? "0" + DateTime.Parse(cm.DtAssembleiaAtual).Day.ToString() : DateTime.Parse(cm.DtAssembleiaAtual).Day.ToString()) + "/" + (DateTime.Parse(cm.DtAssembleiaAtual).Month < 10 ? "0" + DateTime.Parse(cm.DtAssembleiaAtual).Month.ToString() : DateTime.Parse(cm.DtAssembleiaAtual).Month.ToString()) + DateTime.Parse(cm.DtAssembleiaAtual).Year : "");
                    string dtvecto = (cm.Vencto != null && cm.Vencto != "" ? (DateTime.Parse(cm.Vencto).Day < 10 ? "0" + DateTime.Parse(cm.Vencto).Day.ToString() : DateTime.Parse(cm.Vencto).Day.ToString()) + "/" + (DateTime.Parse(cm.Vencto).Month < 10 ? "0" + DateTime.Parse(cm.Vencto).Month.ToString() : DateTime.Parse(cm.Vencto).Month.ToString()) + DateTime.Parse(cm.Vencto).Year : "");

                    model.Endereco = cm.Endereco;
                    model.BemObjeto = cm.BemObjeto;
                    model.Cont = cm.Cont;
                    model.NumeroContrato = cm.NumeroContrato;
                    model.cota = cm.cota;
                    model.cpf = cm.cpf;
                    model.DtAdesao = dtadesao;
                    model.DtAlocacao = dtalocacao;
                    model.DtAssembleia = dtassembleia;
                    model.DtCadastro = dtcadastro;
                    model.DtNascimento = dtnascimento;
                    model.FilialAdm = cm.FilialAdm.ToString();
                    model.FilialVenda = cm.FilialVenda.ToString();
                    model.FndReserva = cm.FndReserva;
                    model.grupo = cm.grupo;
                    model.nome = cm.nome;
                    model.PlanoBasico = cm.PlanoBasico;
                    model.PlanoVenda = cm.PlanoVenda;
                    model.PtoEntrega = cm.PtoEntrega;
                    model.PtoVenda = cm.PtoVenda;
                    model.Seguros = cm.Seguros.ToString();
                    model.sitcobranca = cm.sitcobranca;
                    model.situacao = cm.situacao;
                    model.TaxaAdesao = cm.TaxaAdesao;
                    model.TaxaAdm = cm.TaxaAdm;
                    model.tipocota = cm.tipocota;
                    model.TipoVenda = cm.TipoVenda;
                    model.DtAssembleiaAtual = dtassembleiatual;
                    model.AssembleiaAtual = cm.AssembleiaAtual;
                    model.Vencto = dtvecto;
                    model.ValoresPgos = cm.ValoresPgos;
                    model.SaldoDevedor = cm.SaldoDevedor;
                    model.Atraso = cm.Atraso;
                    model.ValorParcela = cm.ValorParcela;
                    model.NumeroParcel = cm.NumeroParcel;
                    model.QtdPclsPagas = cm.QtdPclsPagas;
                    model.fg_Hidden = cm.situacao == "C" ? "" : "hidden";

                }
            }

            return Json(model);
        }

        public JsonResult PaginaContrato(string contra,string nm, string checkaloc, int pag)
        {
            var busca = c.BuscarContrato(contra, nm, checkaloc);

            return Json(busca.Skip((pag-1) * 5).Take(5).ToList());

        }
    }
}