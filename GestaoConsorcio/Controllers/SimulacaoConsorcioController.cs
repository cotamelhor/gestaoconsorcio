﻿using AnalisadorPerfilConsorcio;
using GestaoConsorcio.Models.Repositorio;
using GestaoConsorcio.Models.View;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Caching;
using System.Runtime.Caching;
using System.Configuration;
using GestaoConsorcio.Models.Cache;

namespace GestaoConsorcio.Controllers
{
    public class SimulacaoConsorcioController : Controller
    {
        SimulacaoBD db = new SimulacaoBD();

        static MemoryCache cache = new MemoryCache("MelhoresProspostasCache");
        CacheItemPolicy policy = new CacheItemPolicy { SlidingExpiration = new TimeSpan(0, Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpire"]), 0) };

        public ActionResult Index()
        {
            return View("~/Views/SimulacaoConsorcio/BuscarSimulacao.cshtml");
        }

        public ActionResult NovaSimulacao()
        {
            return View("~/Views/SimulacaoConsorcio/NovaSimulacao.cshtml");
        }

        [HttpPost]
        public JsonResult BuscarSimulacao(string nome, decimal? valor)
        {
            return Json(db.ListarSimulacoes(nome, valor));
        }

        public ActionResult SimularConsorcio(string ValorCarta, string Parcela, string Lance, bool? IncluirGrupoFormacao)
        {
            if (IncluirGrupoFormacao == null)
            {
                IncluirGrupoFormacao = false;
            }
            var valorCarta = Convert.ToInt32(ValorCarta.Substring(0, ValorCarta.Length - 2).Replace(".", "").Replace(",", ""));
            var parcela = Convert.ToInt32(Parcela.Substring(0, Parcela.Length - 2).Replace(".", "").Replace(",", ""));
            var lance = Convert.ToInt32(Lance.Substring(0, Lance.Length - 2).Replace(".", "").Replace(",", ""));
            ConstrutorProposta construtor = new ConstrutorProposta();
            var list = cache;
            List<BuscarMelhoresPropostasResult> melhoresPropostas = new List<BuscarMelhoresPropostasResult>();

            if (list != null && list.GetCount() > 0)
            {
                foreach (var item in list)
                {
                    if (Convert.ToInt32(item.Key) == valorCarta)
                    {
                        foreach (BuscarMelhoresPropostasResult itemCache in (List<BuscarMelhoresPropostasResult>)item.Value)
                        {
                            if (itemCache.Parcela <= parcela && Convert.ToInt64(itemCache.Lance.ToString().Replace(".", "").Replace(",", "")) <= lance)
                            {
                                itemCache.Valor = Convert.ToInt32(item.Key);
                                melhoresPropostas.Add(itemCache);
                            }
                        }
                    }
                }
            }
            if (melhoresPropostas.Count <= 0)
            {
                melhoresPropostas = construtor.BuscarMelhoresPropostas(valorCarta, parcela, lance, 5, 150, (bool)IncluirGrupoFormacao);
            }

            var hs = valorCarta.GetHashCode();

            cache.Add(hs.ToString(), melhoresPropostas, policy);

            ViewBag.ParametrosValorCarta = ValorCarta;
            ViewBag.ParametrosParcela = Parcela;
            ViewBag.ParametrosLance = Lance;

            return View("~/Views/SimulacaoConsorcio/DetalheSimulacao.cshtml", melhoresPropostas);
        }

        public ActionResult Detalhe(Guid id)
        {
            List<RetornoSimulacaoConsorcio> Simulacao = new List<RetornoSimulacaoConsorcio>();
            Parcelas parcelas = new Parcelas();
            List<Parcelas> parcelasAntes = new List<Parcelas>();
            List<Parcelas> parcelasApos = new List<Parcelas>();
            //TODO: Preencher as propriedades
            parcelas.ParcelaDe = 1;
            parcelas.ParcelaAte = 20;
            parcelas.ValorParcela = 120000;

            parcelasAntes.Add(parcelas);
            parcelasApos.Add(parcelas);

            RetornoSimulacaoConsorcio RetSimCons = new RetornoSimulacaoConsorcio
            (20000000, 25000000, 1500000, 1800, 18, parcelasAntes, parcelasApos, 15.5, 3.2, DateTime.Now, 20, 60);

            var financiamento = new Financiamento(5000000000, 3000000, 35000000, 8000000, 1300000, 200000, 50, 12);

            Simulacao.Add(RetSimCons);

            var detalheSimulacao = new DetalheSimulacao(Simulacao, financiamento);

            return View("~/Views/SimulacaoConsorcio/DetalheSimulacao.cshtml", detalheSimulacao);
        }

    }
}