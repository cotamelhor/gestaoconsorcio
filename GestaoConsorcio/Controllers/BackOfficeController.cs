﻿using GestaoConsorcio.Models.Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;


namespace GestaoConsorcio.Controllers
{
    [Authorize(Roles = "BackOfficeDoc , BackOfficeLig")]

    public class BackOfficeController : Controller
    {
        BackOfficeBD bo = new BackOfficeBD();
        [HttpGet]
        public ActionResult Tarefas()
        {
            string id = "";

            if (User.IsInRole("BackOfficeDoc"))
            {
                var msg = bo.TarefaHojeDoc(id);
                var sol = bo.SolicitacaoCont();
                ViewBag.Retorno = msg.Take(5).ToList();
                ViewBag.CountRetorno = msg.Count;
                ViewBag.Solicitacoes = sol.Take(5).ToList();
                ViewBag.CountSol = sol.Count;
                
            }
            if(User.IsInRole("BackOfficeLig"))
            {
                var msg = bo.TarefaHojeLig(id);
                ViewBag.Retorno = msg.Take(5).ToList();
                ViewBag.Count = msg.Count;
                
            }

            return View();
        }
    }
}
