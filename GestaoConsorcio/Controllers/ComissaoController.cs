﻿using GestaoConsorcio.Models;
using GestaoConsorcio.Models.Repositorio;
using GestaoConsorcio.Models.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using GestaoConsorcio.Models.Banco;
using Microsoft.AspNet.Identity;

namespace GestaoConsorcio.Controllers
{
    [Authorize]
    public class ComissaoController : Controller
    {

        ComissaoBD cobd = new ComissaoBD();

        public ActionResult Cadastrar()
        {
            var model = cobd.Consultores();
            return View(model);
        }

        

        [HttpPost]
        public ActionResult Cadastrar(ComissaoVM model)
        {
            model.ComboConsultor = int.Parse(Request.Form["ComboConsultorNovo"]);
            model.consultor = cobd.Consultores().consultor;
            var salvar = cobd.SalvarComissao(model);

            if (salvar == "1")
            {
                ViewBag.Visivel = "";
                ViewBag.TipoAlert = "alert-success";
                ViewBag.Mensagem = "Salvo com sucesso!";
            }
            else if (salvar == "-1")
            {
                ViewBag.Visivel = "";
                ViewBag.TipoAlert = "alert-danger";
                ViewBag.Mensagem = "Problemas ao salvar comissão , por favor tente novamente!";
            }
            else
            {
                ViewBag.Visivel = "";
                ViewBag.TipoAlert = "alert-info";
                ViewBag.Mensagem = "Consultor já possui comissão";
            }
            return View(model);
        }
    }
}