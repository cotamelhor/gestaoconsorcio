﻿using GestaoConsorcio.Models.Banco;
using GestaoConsorcio.Models.Repositorio;
using GestaoConsorcio.Models.View;
using GestaoConsorcio.Views.Agendamento;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace GestaoConsorcio.Controllers
{
    [Authorize]
    public class AgendamentoController : Controller
    {
        private AgendamentoBD agenbd = new AgendamentoBD();
        private ComboBD comb = new ComboBD();

        [HttpGet]
        public ActionResult AgendamentoLigacao()
        {
            AgendamentosLigacaoVM al = new AgendamentosLigacaoVM();
            var comconsul = comb.ComboConsultorBD();
            al.Consultor = comconsul;
            ViewBag.Visivel = "hidden";
            return View(al);
        }

        [HttpPost]
        public ActionResult AgendamentoLigacao(AgendamentosLigacaoVM model)
        {
            if (User.IsInRole("Atendente"))
            {
                model.IdConsultor = Int32.Parse(User.Identity.GetUserId());
            }
            else
            {
                model.IdConsultor = int.Parse(Request.Form["ComboConsultorNovo"]);
            }

            var comconsul = comb.ComboConsultorBD();
            model.Consultor = comconsul;
            if (!ModelState.IsValid)
            {
                ViewBag.Visivel = "hidden";
                return View(model);
            }

            var salvar = agenbd.SalvarAgendamentoLigacao(model);

            if (salvar == true)
            {
                ViewBag.Visivel = "";
                ViewBag.TipoAlert = "alert-success";
                ViewBag.Mensagem = "Agendamento salvo com sucesso!";
            }
            else
            {
                ViewBag.Visivel = "";
                ViewBag.TipoAlert = "alert-danger";
                ViewBag.Mensagem = "Problemas ao salvar o agendamento, por favor tente novamente!";
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult AgendamentoVendas()
        {
            AgendamentosVendasVM agvendas = new AgendamentosVendasVM();
            var comconsul = comb.ComboConsultorBD();
            agvendas.Consultor = comconsul;
            ViewBag.Visivel = "hidden";

            return View(agvendas);
        }

        [HttpPost]
        public ActionResult AgendamentoVendas(AgendamentosVendasVM model)
        {
            if (User.IsInRole("Atendente"))
            {
                model.IdConsultor = Int32.Parse(User.Identity.GetUserId());
            }
            else
            {
                model.IdConsultor = int.Parse(Request.Form["ComboConsultorNovo"]);
            }
            var comconsul = comb.ComboConsultorBD();
            model.Consultor = comconsul;

            if (!ModelState.IsValid)
            {
                ViewBag.Visivel = "hidden";
                return View(model);
            }

            var salvar = agenbd.SalvarAgendamentoVendas(model);

            if (salvar == true)
            {
                ViewBag.Visivel = "";
                ViewBag.TipoAlert = "alert-success";
                ViewBag.Mensagem = "Agendamento salvo com sucesso!";
            }
            else
            {
                ViewBag.Visivel = "";
                ViewBag.TipoAlert = "alert-danger";
                ViewBag.Mensagem = "Problemas ao salvar o agendamento, por favor tente novamente!";
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult Chat(string agendamento)
        {
            var buscaprotocolo = agenbd.BuscaProtocolo(agendamento);
            ConsultaChatLib.Chat chat = (new ConsultaChatLib.Robo()).BuscarDadosChat(buscaprotocolo);

            ChatVM cv = new ChatVM();
            cv.AgendamentoId = agendamento;
            cv.Cliente = chat.Cliente;
            cv.Conversa = chat.Conversa;
            cv.Duracao = chat.Duracao;
            cv.IP = chat.IP;
            cv.Protocolo = chat.Protocolo;
            cv.Resumo = chat.Resumo;
            cv.Sessao = chat.Sessao;

            return View("Chat", cv);
        }

        public ActionResult Agendamento()
        {
            string id = "";

            if (User.IsInRole("Atendente"))
            {
                id = User.Identity.GetUserId();
            }

            var msg = agenbd.BuscarAgendamentoBD(id, "", "", "", "", false, 0, 0, 0);

            ViewBag.Retorno = msg.Take(15).ToList();
            ViewBag.Count = msg.Count;

            return View();
        }

        public ActionResult NovoAgendamento()
        {
            ViewBag.fg_Hidden = "hidden";
            return View();
        }

        public JsonResult SalvarAgendamento(AgendamentosVM lista)
        {
            var msg = "";
            if (User.IsInRole("Atendente"))
            {
                lista.agendamento[0].consultor = Int32.Parse(User.Identity.GetUserId());
            }

            msg = agenbd.SalvarAgendamentoBD(lista);

            return Json(msg);
        }

        public JsonResult ExcluirAgendamento(int id)
        {
            var msg = agenbd.ExcluirAgendamentoBD(id);

            return Json(msg);
        }

        public JsonResult ReativarAgendamento(int id, string status)
        {
            var msg = agenbd.FinalizarAgendamentoBD(id, status, false);

            return Json(msg);
        }

        public JsonResult FinalizarAgendamento(int id, string status)
        {
            var msg = agenbd.FinalizarAgendamentoBD(id, status);

            return Json(msg);
        }

        public JsonResult TarefaHoje(int pag)
        {
            string id = "";

            Tipo tipo = new Tipo();

            if (User.IsInRole("Atendente"))
            {
                id = User.Identity.GetUserId();
            }

            var msg = agenbd.BuscarAgendamentoBD(id, "", "", "", "", false, 0, 0, 0);

            return Json(msg.Skip((pag - 1) * 5).Take(5).ToList());
        }

        public JsonResult BuscarAgendamento(string consultor, string tipo, string cliente, string data, string dataate, int pag, bool finalizado, int cor, int valini, int valfim)
        {
            if (consultor == "undefined" || consultor == "")
            {
                consultor = User.Identity.GetUserId();
            }

            var busca = agenbd.BuscarAgendamentoBD(consultor, tipo, cliente, data, dataate, finalizado, cor, valini, valfim);

            Retorno rt = new Retorno();

            rt.consultor = consultor;
            rt.tipo = tipo;
            rt.cliente = cliente;
            rt.data = data;
            rt.dataate = dataate;
            rt.quantidade = busca.Count;
            rt.dados = busca.Skip((pag - 1) * 15).Take(15).ToList();

            return Json(rt);
        }

        [HttpGet]
        public ActionResult DetalheAgendamento(string url)
        {
            DadosAgen detalhe = agenbd.DetalheAgendamentoBD(url);
            ViewBag.Contatos = detalhe.contatos;
            ViewBag.IDAgendamento = detalhe.agendamentoid;
            ViewBag.TipoPessoa = detalhe.tiporadiopessoa;
            ViewBag.fg_Hidden = "hidden";
            if (detalhe.estadocivilf == 2)
            {
                ViewBag.ECivil = "";
            }
            else
            {
                ViewBag.ECivil = "hidden";
            }
            return View("DetalheAgendamento", detalhe);
        }

        public JsonResult SalvarAlteracao(AgendamentosVM lista)
        {
            if (User.IsInRole("Atendente") || lista.agendamento[0].consultor == null)
            {
                lista.agendamento[0].consultor = Int32.Parse(User.Identity.GetUserId());
            }

            lista.agendamento[0].UltimoContato = User.Identity.GetUserId<int>();

            var alt = agenbd.SalvarAlteracaoBD(lista);

            return Json(alt);
        }

        public JsonResult VerificarLigacaoAgendada()
        {
            var res = agenbd.VerificarAgendamento(User.Identity.GetUserId<int>());
            var ret = res.FirstOrDefault();

            if (ret == null) return null;

            var obj = new
            {
                Nome = ret.nome,
                Fone1 = ret.fone1,
                Fone2 = ret.fone2,
                DataHora = ret.dtcontato,
                AgendamentoId = ret.agendamentoid
            };

            return Json(obj);
        }

        [HttpPost]
        public JsonResult BuscaHistoricoAgendamento(BuscarAgendamentoHistoricoVM requestModel)
        {
            try
            {
                IEnumerable<AgendamentoHistorico> response = new List<AgendamentoHistorico>();

                if (User.IsInRole("Atendente"))
                {
                    requestModel.ConsultorId = 0;
                    var userId = User.Identity.GetUserId<int>();

                    response = agenbd.ListaHistoricoAgendamento(requestModel, userId);
                }
                else
                {
                    response = agenbd.ListaHistoricoAgendamento(requestModel);
                }

                if (response != null && response.Any())
                {
                    var historicoVm = response.Select(x =>
                     new AgendamentoHistoricoVM
                     {
                         Cliente = x.Agendamento.Nome,
                         CriadoEm = x.CriadoEm.ToString(),
                         Id = x.Id,
                         TipoFinal = x.TipoFinal,
                         TipoInicial = x.TipoInicial
                     });
                    return Json(new { Data = historicoVm });
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json(new { Message = "Não foi possivel recuperar historico deste agendamento" });
            }
        }

        [HttpGet]
        public ActionResult HistoricoAgendamento()
        {
            try
            {
                IEnumerable<AgendamentoHistorico> historico = new List<AgendamentoHistorico>();

                if (User.IsInRole("Atendente"))
                {
                    var userId = User.Identity.GetUserId<int>();

                    historico = agenbd.ListaAgendamentosHistoricoPorUserId(userId);
                }
                else
                {
                    historico = agenbd.ListaHistoricoAgendamento();
                }

                var historicoVm = historico.Select(x =>
                   new AgendamentoHistoricoVM
                   {
                       Cliente = x.Agendamento.Nome,
                       CriadoEm = x.CriadoEm.ToString(),
                       Id = x.Id,
                       TipoFinal = x.TipoFinal,
                       TipoInicial = x.TipoInicial
                   });

                return View("AgendamentoHistorico", historicoVm.ToList());
            }
            catch (Exception ex)
            {
                return Json(new { Message = "Não foi possivel recuperar historico deste agendamento" });
            }
        }

        [HttpPost]
        public JsonResult BuscaTipoAgendamento()
        {
            try
            {
                var tiposAgendamento = agenbd.ListarTiposAgendamento();

                return Json(tiposAgendamento);
            }
            catch (Exception)
            {
                return Json(new { Message = "Não foi possivel recuperar os tipos de agendamento" });
            }
        }
    }
}