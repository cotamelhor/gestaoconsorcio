﻿using GestaoConsorcio.Models.Banco;
using GestaoConsorcio.Models.Repositorio;
using GestaoConsorcio.Models.Util;
using GestaoConsorcio.Models.View;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestaoConsorcio.Controllers
{
    [Authorize]
    public class CampanhasController : Controller
    {
        private CampanhasBD bdcamp = new CampanhasBD();

        public ActionResult Index()
        {
            int qtde = 0;

            if (User.IsInRole("Administrador") || User.IsInRole("BackOfficeDoc") || User.IsInRole("BackOfficeLig") || User.IsInRole("Caixa"))
            {
                qtde = bdcamp.ContarCampanhas();
            }
            else
            {
                qtde = bdcamp.ContarCampanhas(usuid: int.Parse(User.Identity.GetUserId()));
            }

            ViewBag.Count = qtde;
            ViewBag.Usuario = User.Identity.GetUserId();

            return View();
        }

        [Route("exportar-consultores")]
        [HttpGet]
        public ActionResult ExportarRelatorio(string nomeOuEmail = "", string grupo = "",
            string dataInicio = null, string dataFim = null, string campoData = "lead", string ordenacao = "desc", int registros = 20, bool somentePresidentes = false, int presidente = 0)
        {
            List<ContatoCampanha> ret = null;

            if (User.IsInRole("Administrador") || User.IsInRole("BackOfficeDoc") || User.IsInRole("BackOfficeLig") || User.IsInRole("Caixa"))
            {
                ret = bdcamp.BuscarContatosCampanhas(0);
            }
            else
            {
                ret = bdcamp.BuscarContatosCampanhas(int.Parse(User.Identity.GetUserId()));
            }

            var relatorio = ret.Select(x => new {
               Nome = x.Nome,
               Telefone = x.Telefone,
               Email = x.Email,
               UF = x.Estado,
               Data = x.CriadoEm.ToString("dd/MM/yyy HH:mm:ss"),
               UltimaAcao = x.ObservacaoContatoes.Count() > 0 ? ((DateTime)x.ObservacaoContatoes.Max(o => o.CriadoEm)).ToString("dd/MM/yyy HH:mm:ss") : null,
               ValorDaCarta = x.ValoresCarta,
               Renda = x.Renda,
               MelhorHorario = x.MelhorHorario,
               Status = x.UltimaAcaoDesc,
               PropostaEnviada = x.CCampTags.Any(o=>o.TagContatoFK == 8)?"Sim":"Não",
               Finalizado = x.CCampTags.Any(o => o.TagContatoFK == 6) ? "Sim" : "Não",
            }).ToList();

            var nomeArquivo = "RelatorioConsultores" + DateTime.Now.ToString("dd/MM/yyy HH:mm:ss") + ".xlsx";
            var fileContent = EppExcelExport.ExportExcel(relatorio);
            return File(fileContent, EppExcelExport.ExcelContentType, nomeArquivo);
        }

        public ActionResult Detalhe(int contatoid)
        {
            var Model = bdcamp.BuscarContatoPorID(contatoid);
            var obss = bdcamp.ListarObservacoes(contatoid);

            ViewBag.Lista = obss.Select(x => new ObsCCampVM() {
                                Id = x.ObservacaoContatoID,
                                Consultor = x.Usuario == null ? "" : x.Usuario.Nome,
                                Texto = x.Texto,
                                Data = x.CriadoEm
                            }).ToList();

            ViewBag.Invalido = bdcamp.VerificarTagContato(contatoid, 2)? "checked":"";
            ViewBag.CxPostal = bdcamp.VerificarTagContato(contatoid, 7) ? "checked" : "";
            ViewBag.Incompativel = bdcamp.VerificarTagContato(contatoid, 11) ? "checked" : "";
            ViewBag.Pesquisando = bdcamp.VerificarTagContato(contatoid, 9) ? "checked" : "";
            ViewBag.Enviada = bdcamp.VerificarTagContato(contatoid, 8) ? "checked" : "";
            ViewBag.Finalizado = bdcamp.VerificarTagContato(contatoid, 6) ? "checked" : "";
            ViewBag.Desconto = bdcamp.VerificarTagContato(contatoid, 10) ? "checked" : "";
            

            ViewBag.ListaConsultores = new SelectList(
                                            new ComboBD().ComboConsultorBD(),
                                            "consultorid",
                                            "nome"
                                        );

            return View(Model);
        }

        [HttpPost]
        public ActionResult Gravar(ContatoCampanha dados)
        {
            bdcamp.Salvar(dados);

            return RedirectToAction("Index");
        }

        private DateTime? String2Date(string texto)
        {
            try
            {
                if (string.IsNullOrEmpty(texto)) return null;

                var list = texto?.Split('-');
                if (list.Count() > 2)
                {
                    return new DateTime(Int32.Parse(list[0]), Int32.Parse(list[1]), Int32.Parse(list[2]));
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }

        }

        public JsonResult BuscarCampanha(int pag = 0, int usuid = 0, int campanhafk = 0, string inicio = null, string fim = null)
        {
            if (usuid == 0)
            {
                usuid = Int32.Parse(User.Identity.GetUserId());
            }
            
            var busca = bdcamp.BuscarContatosCampanhas(pag, usuid, campanhafk, String2Date(inicio), String2Date(fim));

            var lista = busca.Select(x => new
            {
                ContatoCampanhaID = x.ContatoCampanhaID,
                UltimaAcao = x.ObservacaoContatoes.Count() > 0 ? x.ObservacaoContatoes.Max(o => o.CriadoEm.ToString("dd/MM/yyy HH:mm:ss")) : " - ",
                Estado = x.Estado ?? " - ",
                CriadoEm = x.CriadoEm.ToString("dd/MM/yyy HH:mm:ss"),
                Nome = x.Nome ?? " - ",
                Telefone = x.Telefone ?? " - ",
                ValoresCarta = x.ValoresCarta ?? " - ",
                UltimaAcaoDesc = x.UltimaAcaoDesc ?? " - "
            }).ToList();


            return Json(lista);

        }

        [HttpGet]
        public ActionResult Importacao()
        {
            return View(new CampanhaImportacaoVM());
        }


        [HttpPost]
        public ActionResult Importacao(CampanhaImportacaoVM model)
        {
            if (model.CampanhaID == 0 || model.ComboConsultorID == 0)
            {
                return View(model);
            }

            DataTable tab = GetTable(model.Texto);

            bdcamp.SalvarImportacao(tab, model.ComboConsultorID, model.CampanhaID);

            return RedirectToAction("Index");
        }

        private DataTable GetTable(string texto)
        {
            bool first = true;
            string[] linhas = texto.Replace("\r", "").Split('\n');
            DataTable table = new DataTable();

            foreach (string linha in linhas)
            {
                string[] colunas = linha.Split(';');
                if (first)
                {
                    foreach (string nomecol in colunas)
                    {
                        table.Columns.Add(nomecol, typeof(string));
                    }
                    first = false;
                }
                else
                {
                    table.Rows.Add(colunas);
                }
            }

            return table;
        }


        [HttpPost]
        public ActionResult SalvarObs(string txt, int contatoid, DateTime? data)
        {
            bdcamp.SalvarObs(int.Parse(User.Identity.GetUserId()), contatoid, txt, data??DateTime.Now);

            return null;
        }

        public ActionResult SalvarTagContato(int contatoid, int tagid, bool status)
        {
            if (status)
            {
                bdcamp.AssociarTagContato(contatoid, tagid);
            }
            else
            {
                bdcamp.ApagarTagContato(contatoid, tagid);
            }

            return null;
        }

        public ActionResult SalvarTagObs(int obsid, int tagid, int status)
        {
            //bdcamp.SalvarObs(int.Parse(User.Identity.GetUserId()), contatoid, txt);

            return null;
        }

    }
}