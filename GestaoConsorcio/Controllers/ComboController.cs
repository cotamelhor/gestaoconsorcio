﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestaoConsorcio.Models.Repositorio;

namespace GestaoConsorcio.Controllers
{
    [Authorize]
    public class ComboController : Controller
    {
        ComboBD comb = new ComboBD();

        public JsonResult TipoConsorcio()
        {
            var tipocon = comb.ComboTipoConsorcio();
            return Json(tipocon);
        }

        public JsonResult ComboConsultor()
        {
            var comconsul = comb.ComboConsultorBD();
            return Json(comconsul);
        }

        public JsonResult ComboEstadoCivil()
        {
            var comestadocivil = comb.ComboEstadoCivilBD();
            return Json(comestadocivil);
        }
        
    }
}