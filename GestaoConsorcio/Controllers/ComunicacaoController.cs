﻿using GestaoConsorcio.Models.Repositorio;
using GestaoConsorcio.Models.Servicos;
using GestaoConsorcio.Models.View;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestaoConsorcio.Controllers
{
    [Authorize]
    public class ComunicacaoController : Controller
    {
        private ComunicacaoBD combd = new ComunicacaoBD();

        // GET: Comunicacao
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult EnviarSms(EnviarSmsVM model)
        {
            combd.SalvarLogSms(Int32.Parse(User.Identity.GetUserId()), model);
            SmsService service = new SmsService();
            service.EnviarSmsSenha(model.ListaTelefones, model.Mensagem);
            return View();
        }
    }
}