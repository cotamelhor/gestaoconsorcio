﻿setInterval(function () {
    $.ajax({

        type: 'post',
        cache: false,
        contentType: 'application/json; charset=utf-8',
        url: '/Agendamento/VerificarLigacaoAgendada',
        accepts: {
            text: "application/json"
        },
        success: function (dados) {

            console.log(JSON.stringify(dados));

            if (dados != "") {
                var msg = "ATENÇÃO!!! \r\n Você tem uma ligação agendada. \r\n Nome: "
                    + dados.Nome + " \r\n Fone: " + dados.Fone1
                    + " \r\n Tratar agora?"

                var r = confirm(msg);
                if (r == true) {

                    var href = "../Agendamento/DetalheAgendamento?url=" + dados.AgendamentoId + "";
                    window.location.href = href;

                }
            }

        },
        error: function (erro) {
            console.log("Erro" + JSON.stringify(erro));
        }
    })
}, 50000);