﻿$(document).ready(function () {

    var contfirst = $(".divcontador").attr('id');
    var contsec = contfirst;

    $(".btnreativar").click(function () {

        console.log('btnreativar');

        var idagend = $(".dividagendamento").attr('id');
        var status = $('.radioFinaliza:checked').val();

        $.ajax({

            type: 'post',
            cache: false,
            contentType: 'application/json; charset=utf-8',
            url: '/Agendamento/ReativarAgendamento',
            data: "{ 'id': '" + idagend + "','status':'" + status + "'}",
            success: function (json) {

                if (json == "false") {
                    $(".modalbodymsg").html("<strong>Não foi possível finalizar o seu agendamento</strong>");
                    $('.btnokmodal').attr('onclick', ' ');
                    $(".modalmsg").modal('show');
                }
                if (json == "true") {
                    $(".modalbodymsg").html("<strong>Agendamento Finalizado!</strong>");
                    $('.btnokmodal').attr('onclick', ' ');
                    $(".modalmsg").modal('show');

                }

            }
        });

    })


    $(".btncancelagendamento").click(function () {
            
        var idagend = $(".dividagendamento").attr('id');
        var status = $('.radioFinaliza:checked').val();

        $.ajax({

            type: 'post',
            cache: false,
            contentType: 'application/json; charset=utf-8',
            url: '/Agendamento/FinalizarAgendamento',
            data: "{ 'id': '" + idagend + "','status':'"+ status +"'}",
            success: function (json) {

                if (json == "false") {
                    $(".modalbodymsg").html("<strong>Não foi possível finalizar o seu agendamento</strong>");
                    $('.btnokmodal').attr('onclick', ' ');
                    $(".modalmsg").modal('show');
                }
                if (json == "true") {
                    $(".modalbodymsg").html("<strong>Agendamento Finalizado!</strong>");
                    $('.btnokmodal').attr('onclick', ' ');
                    $(".modalmsg").modal('show');

                }

            }
        });
        
    })

    $(".btnsalvaralter").click(function () {

        var classificacao = $("#ClassificacaoV").val();
        var dtnascimentoconjugue = "";
        var dtnascimento = "";
        var numcontrato = $("#numcontrato").val();
        var protocolo = $("#protocolo").val();
        var agendamentoid = $(".dividagendamento").attr('id');
        var tiporadiopessoa = $('.radiopessoa:checked').val();
        var fg_urgente = $('#CheckUrgente').is(":checked");
        var nome = $("#nomecliente").val();
        var fone1 = $("#novofone1").val();
        var fone2 = $(".novofone2").val();
        var email = $("#novoemail").val();
        var dtatendimento = $("#novodtatentimento").val();
        var consultor = $("#ComboConsultorNovo").val();
        var valorauto = "";
        var valorimo = "";
        var cpff = "";
        var nomef = "";
        var rgf = "";
        var orgaoemissorf = "";
        var dtexpedicao = "";
        var enderecof = "";
        var bairrof = "";
        var cidadef = "";
        var uff = "";
        var estadocivilf = "";
        var nomeconjugue = "";
        var cpfconjugue = "";
        var profissaof = "";
        var cnascimentof = "";
        var nacionalidadef = "";
        var fonefixof = "";
        var celularf = "";
        var emailf = "";
        var bancof = "";
        var agenciaf = "";
        var contaf = "";
        var formapagamentof = "";
        var cnpjj = "";
        var razaosocialj = "";
        var nomefantasiaj = "";
        var fundacaoj = "";
        var capitalsocialj = "";
        var tipocapitalj = "";
        var atividadej = "";
        var nomesocioj = "";
        var cargosocioj = "";
        var particsocioj = "";
        var cepj = "";
        var ruaj = "";
        var bairroj = "";
        var cidadej = "";
        var ufj = "";
        var estadocivilj = "";
        var fonefixoj = "";
        var celularj = "";
        var emailj = "";
        var bancoj = "";
        var agenciaj = "";
        var contaj = "";
        var formapagamentoj = "";
        var cpfsocioj = "";
        var cepf = "";
        var autocheck = false;
        var imocheck = false;
        var tipocliente = $("#ComboTipoCliente").val();
        var dtcontatos = "";
        var numerof = "";
        var numeroj = "";
        var complemento = "";
        var finalizado = false;
        var Operacaof = "";
        var Operacaoj = "";
        var AgendamentoSituacaoFK = "";

        dtcontatos = { contatos: [], agendamento: [] };

        if ($(".checkautonovo").is(":checked")) {
            autocheck = true;
            valorauto = $(".novovalauto").val();

        }


        if ($(".checkimonovo").is(":checked")) {

            imocheck = true;
            valorimo = $(".novovalimoo").val();

        }


        if (contsec != 0)
        {    
            var qtdcontatos = contsec;

            for (i = 1; i <= contsec; i++) {

                var veri = $(".novocontdata" + i + "").val();

                if (veri != "" && veri != null) {

                    var contid = $(".novocontdata" + i + "").attr('id');
                    var hora = $(".novohora" + i + "").val();
                    var dtc = $(".novocontdata" + i + "").val();
                    var obsc = $(".novocontobs" + i + "").val();
                    dtcontatos.contatos.push({ contfirst: contfirst, contsec: contsec, contadoid: contid, dtcontato: dtc, observacao: obsc, hora: hora });

                }
                




            }
        }


        if (tiporadiopessoa == "pfisica") {
            Operacaof = $(".novoopef").val();
            complemento = $(".novocomplementof").val();
            dtnascimento = $(".novodtnascimentof").val();
            cpff = $(".novocpff").val();
            rgf = $(".novorgf").val();
            orgaoemissorf = $(".novoemissorf").val();
            cepf = $(".novocepf").val();
            enderecof = $(".novoenderecof").val();
            bairrof = $(".novobairrof").val();
            cidadef = $(".novocidadef").val();
            AgendamentoSituacaoFK = $("#AgendamentoSituacaoFK").val();
            uff = $(".novouff").val();
            estadocivilf = $("#ComboEstadoCivilNovo").val();
            dtexpedicao = $(".novodtexpedicaof").val();

            if ($('#ComboEstadoCivilNovo').val() == "2") {
                nomeconjugue = $(".novonomeconjuguef").val();
                cpfconjugue = $(".novocpfconjuguef").val();
                dtnascimentoconjugue = $(".novodtnascimentoconjuguef").val();
            }

            profissaof = $(".novoprofissaof").val();
            cnascimentof = $(".novocidadenascf").val();
            nacionalidadef = $(".novonaciof").val();
            fonefixof = $(".novofonef").val();
            celularf = $(".novocelf").val();
            emailf = $(".novoemailf").val();
            bancof = $(".novobancof").val();
            agenciaf = $(".novoagenciaf").val();
            contaf = $(".novocontaf").val();
            formapagamentof = $('.formpagamentof:checked').val();
            numerof = $(".novonumerof").val();

        }
        if (tiporadiopessoa == "pjuridica") {

            Operacaoj = $(".novoopej").val();
            complemento = $(".novocomplementoj").val();
            dtnascimento = $(".novodtnascimentof").val();
            cnpjj = $(".novocnpjj").val();
            razaosocialj = $(".novorazaosocialj").val();
            nomefantasiaj = $(".novonomefantasiaj").val();
            fundacaoj = $(".novofundacaoj").val();
            capitalsocialj = $(".novocapitalj").val();
            tipocapitalj = $(".novotipocapitalj").val();
            atividadej = $(".novoatividadej").val();
            nomesocioj = $(".novonomesocioj").val();
            cargosocioj = $(".novocargosocioj").val();
            cpfsocioj = $(".novocpfsocioj").val();
            particsocioj = $(".novoparticipacaoj").val();
            cepj = $(".novocepj").val();
            AgendamentoSituacaoFK = $("#AgendamentoSituacaoFK").val();
            ruaj = $(".novoruaj").val();
            bairroj = $(".novobairroj").val();
            cidadej = $(".novocidadej").val();
            ufj = $(".novoufj").val();
            estadocivilj = $(".novoestadocivilj").val();
            fonefixoj = $(".novofonej").val();
            celularj = $(".novocelularj").val();
            emailj = $('.novoemailj').val();
            bancoj = $(".novobancoj").val();
            agenciaj = $(".novoagenciaj").val();
            contaj = $(".novocontaj").val();
            formapagamentoj = $('.formpagamentoj:checked').val();
            numeroj = $(".novonumeroj").val();
        }


        dtcontatos.agendamento.push({
            Operacaof: Operacaof,
            Operacaoj: Operacaoj,
            finalizado: finalizado,
            fg_urgente: fg_urgente,
            protocolo: protocolo,
            numcontrato: numcontrato,
            agendamentoid : agendamentoid,
            nome: nome,
            dtexpedicao: dtexpedicao,
            fone1: fone1,
            fone2: fone2,
            email: email,
            dtatendimento: dtatendimento,
            consultor: consultor,
            valorauto: valorauto,
            valorimo: valorimo,
            cpff: cpff,
            nomef: nomef,
            rgf: rgf,
            orgaoemissorf: orgaoemissorf,
            enderecof: enderecof,
            numerof: numerof,
            bairrof: bairrof,
            cidadef: cidadef,
            uff: uff,
            estadocivilf: estadocivilf,
            nomeconjugue: nomeconjugue,
            cpfconjugue: cpfconjugue,
            profissaof: profissaof,
            cnascimentof: cnascimentof,
            nacionalidadef: nacionalidadef,
            fonefixof: fonefixof,
            celularf: celularf,
            emailf: emailf,
            bancof: bancof,
            agenciaf: agenciaf,
            contaf: contaf,
            formapagamentof: formapagamentof,
            cepf: cepf,
            cnpjj: cnpjj,
            razaosocialj: razaosocialj,
            nomefantasiaj: nomefantasiaj,
            fundacaoj: fundacaoj,
            capitalsocialj: capitalsocialj,
            tipocapitalj: tipocapitalj,
            atividadej: atividadej,
            nomesocioj: nomesocioj,
            cargosocioj: cargosocioj,
            particsocioj: particsocioj,
            cepj: cepj,
            AgendamentoSituacaoFK: AgendamentoSituacaoFK,
            ruaj: ruaj,
            bairroj: bairroj,
            numeroj:numeroj,
            cidadej: cidadej,
            ufj: ufj,
            estadocivilj: estadocivilj,
            fonefixoj: fonefixoj,
            celularj: celularj,
            emailj: emailj,
            agenciaj : agenciaj,
            bancoj: bancoj,
            contaj: contaj,
            formapagamentoj: formapagamentoj,
            cpfsocioj: cpfsocioj,
            tiporadiopessoastring: tiporadiopessoa,
            autocheck: autocheck,
            imocheck: imocheck,
            tipocliente: tipocliente,
            dtnascimentoconjugue: dtnascimentoconjugue,
            dtnascimento: dtnascimento,
            complemento: complemento,
            Classificacao: classificacao
        });

        dtcontatos = JSON.stringify(dtcontatos);
        
        
        $.ajax({

            type: 'post',
            cache: false,
            contentType: 'application/json; charset=utf-8',
            url: '/Agendamento/SalvarAlteracao',
            data: dtcontatos,
            success: function (json) {

                if (json == "-1") {
                    $(".modalbodymsg").html("<strong>Por favor, preencher o Nome, Data de Atendimento e selecionar o Consultor</strong>");
                    $('.btnokmodal').attr('onclick', ' ');
                    $(".modalmsg").modal('show');
                }
                if (json == "1") {
                    $(".modalbodymsg").html("<strong>Salvo com sucesso!</strong>");
                    $('.btnokmodal').attr('onclick', ' ');
                    $(".modalmsg").modal('show');

                }
                if (json == "0") {
                    $(".modalbodymsg").html("<strong>Erro ao salvar o agendamento</strong>");
                    $('.btnokmodal').attr('onclick', ' ');
                    $(".modalmsg").modal('show');
                }

            }


        });




    });

    $(".formcontatos").html($(".divconteudocontados").attr('id'));

    $(".btnovocontatoalter").click(function () {

        contsec++;
        var text ="<div class='row'>"+
                   "<div class='col-md-4'>" +
                       "<label>Data</label>" +
                       "<input type='date' class='form-control novocontdata" + contsec + "'>" +
                  "</div>" +
                  "<div class='col-md-2'>" +
                       "<label>Hora</label>"+
                       "<input type='time' class='form-control novohora" + contsec +"'>"+
                  "</div>" +
                  "</div>" +
                  "<br />"+
                  "<div class='row'>" +
                  "<div class='col-md-10'>" +
                      "<label>Observação</label>" +
                      "<textarea class='form-control  novocontobs" + contsec + "' rows='10' maxlength='5000'></textarea>" +
                  "</div>" +
                "</div>"+
                 "<br />";

        $('.formcontatos').append(text);
        
    });
});