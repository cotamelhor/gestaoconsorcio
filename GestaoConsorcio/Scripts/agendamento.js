﻿var consultorex = "";
var tipoex = "";
var clienteex = "";
var aex = "";
var dataex = "";
var datate = "";
var fina = false;
var vlcor = 0;
var vlini = 0;
var vlfin = 0;

$(document).ready(function () {
    var aux = 1;
    var tiporadiopessoa = "";

    $('.btneditdetalhe').click(function () {
        var id = $(this).attr("id");
        var href = "../Agendamento/DetalheAgendamento?url=" + id + "";
        window.open(href, '_blank');
        return false;
    });

    $('.btnexcluidetalhegeral').click(function () {

        $(".modalmsgexcluir").modal('show');
        var id = $(this).attr('id');
        $('.btnexcluiragend').attr('id', id);
        $('.btnexcluiragend').attr('data-name', "tarefahoje");
    });
    
    $('.btnovoagend').click(function () {

        aux = 1;
    });

    

    $('.radiopessoa').click(function () {
        tiporadiopessoa = $('.radiopessoa:checked').val();
        if (tiporadiopessoa == "pfisica") {
            $('.formfisica').show();
            $('.formjuridica').hide();
        } else if (tiporadiopessoa == "pjuridica") {
            $('.formfisica').hide();
            $('.formjuridica').show();
        }
    });

    $('#ComboEstadoCivilNovo').change(function () {
        if ($(this).val() == "2")
        {
            $(".divnomeconjugue").show();
            $(".divcpfconjugue").show();
            $(".divnasciconjugue").show();
            $(".estadocivildiv").show();
            
        } else {
            $(".divnomeconjugue").hide();
            $(".divcpfconjugue").hide();
            $(".divnasciconjugue").hide();
            $(".estadocivildiv").hide();
        }
    });

    $('.btnsalvaragend').click(function () {
        tiporadiopessoa = $('.radiopessoa:checked').val();

        
        var protocolo = $("#protocolo").val();
        var numcontraro = $("#numerocontrato").val();
        var nome = $("#nomecliente").val();
        var fone1 = $("#novofone1").val();
        var fone2 = $(".novofone2").val();
        var email = $("#novoemail").val();
        var dtatendimento = $("#novodtatentimento").val();
        var consultor = $("#ComboConsultorNovo").val();
        var valorauto = "";
        var valorimo = "";
        var cpff = "";
        var nomef = "";
        var rgf = "";
        var orgaoemissorf = "";
        var dtexpedicao = "";
        var enderecof = "";
        var bairrof = "";
        var cidadef = "";
        var uff = "";
        var estadocivilf = "";
        var nomeconjugue = "";
        var dtnascimentoconjugue = "";
        var cpfconjugue = "";
        var profissaof = "";
        var cnascimentof = "";
        var nacionalidadef ="";
        var fonefixof = "";
        var celularf = "";
        var emailf = "";
        var bancof = "";
        var agenciaf = "";
        var contaf = "";
        var formapagamentof = "";
        var cnpjj = "";
        var razaosocialj = "";
        var nomefantasiaj = "";
        var fundacaoj = "";
        var capitalsocialj = "";
        var tipocapitalj = "";
        var atividadej = "";
        var nomesocioj = "";
        var cargosocioj = "";
        var particsocioj = "";
        var complemento = "";
        var cepj = "";
        var ruaj = "";
        var bairroj = "";
        var cidadej = "";
        var ufj = "";
        var estadocivilj = "";
        var fonefixoj = "";
        var celularj = "";
        var emailj = "";
        var bancoj = "";
        var agenciaj = "";
        var contaj = "";
        var formapagamentoj = "";
        var cpfsocioj = "";
        var cepf = "";
        var autocheck = false;
        var imocheck = false;
        var tipocliente = $("#ComboTipoCliente").val();
        var dtcontatos = "";
        var numerof = "";
        var numeroj = "";
        var dtnascimento = "";

        dtcontatos = { contatos: [], agendamento: [] };

        if ($(".checkautonovo").is(":checked"))
        {
            autocheck = true;
            valorauto = $(".novovalauto").val();
         
        }


        if ($(".checkimonovo").is(":checked")) {
            
            imocheck = true;
            valorimo = $(".novovalimoo").val();
  
        }

        
        var qtdcontatos = aux;
        

       

        var incre = "";

        for (i = 1; i <= qtdcontatos; i++) {
            
            var veri = $(".novocontdata" + i + "").val();

            if (veri != "" && veri != null) {

                var hora = $(".novohora" + i + "").val();
                var dtc = $(".novocontdata" + i + "").val();
                var obsc = $(".novocontobs" + i + "").val();

                dtcontatos.contatos.push({ dtcontato: dtc, observacao: obsc, hora: hora });
            }
                
        
        }
        
        

        if (tiporadiopessoa == "pfisica")
        {
            dtnascimento = $(".novodtnascimentof").val();
            cpff = $(".novocpff").val();
            rgf = $(".novorgf").val();
            orgaoemissorf = $(".novoemissorf").val();
            cepf = $(".novocepf").val();
            enderecof = $(".novoenderecof").val();
            bairrof = $(".novobairrof").val();
            cidadef = $(".novocidadef").val();
            uff = $(".novouff").val();
            estadocivilf = $("#ComboEstadoCivilNovo").val();
            dtexpedicao = $(".novodtexpedicaof").val();
            complemento = $(".novocomplementof").val();
            if($('#ComboEstadoCivilNovo').val() == "2")
            {
                nomeconjugue = $(".novonomeconjuguef").val();
                cpfconjugue = $(".novocpfconjuguef").val();
                dtnascimentoconjugue = $(".novodtnascimentoconjuguef").val();
            }

            profissaof = $(".novoprofissaof").val();
            cnascimentof = $(".novocidadenascf").val();
            nacionalidadef = $(".novonaciof").val();
            fonefixof = $(".novofonef").val();
            celularf = $(".novocelf").val();
            emailf = $(".novoemailf").val();
            bancof = $(".novobancof").val();
            agenciaf = $(".novoagenciaf").val();
            contaf = $(".novocontaf").val();
            formapagamentof = $('.formpagamentof:checked').val();
            numerof = $(".novonumerof").val();

           
        }
        if (tiporadiopessoa == "pjuridica")
        {
            dtnascimento = $(".novodtnascimentof").val();
            cnpjj = $(".novocnpjj").val();
            razaosocialj = $(".novorazaosocialj").val();
            nomefantasiaj = $(".novonomefantasiaj").val();
            fundacaoj = $(".novofundacaoj").val();
            capitalsocialj = $(".novocapitalj").val();
            tipocapitalj = $(".novotipocapitalj").val();
            complemento = $(".novocomplementoj").val();
            atividadej = $(".novoatividadej").val();
            nomesocioj = $(".novonomesocioj").val();
            cargosocioj = $(".novocargosocioj").val();
            cpfsocioj = $(".novocpfsocioj").val();
            particsocioj = $(".novoparticipacaoj").val();
            cepj = $(".novocepj").val();
            ruaj = $(".novoruaj").val();
            bairroj = $(".novobairroj").val();
            cidadej = $(".novocidadej").val();
            ufj = $(".novoufj").val();
            estadocivilj = $(".novoestadocivilj").val();
            fonefixoj = $(".novofonej").val();
            celularj = $(".novocelularj").val();
            emailj = $('.novoemailj').val();
            bancoj = $(".novobancoj").val();
            agenciaj = $(".novoagenciaj").val();
            contaj = $(".novocontaj").val();
            formapagamentoj = $('.formpagamentoj:checked').val();
            numeroj = $(".novonumerof").val();
        }

        

        dtcontatos.agendamento.push({
            protocolo: protocolo,
            numcontrato: numcontraro,
            nome: nome,
            dtexpedicao: dtexpedicao,
            fone1: fone1,
            fone2: fone2,
            email: email,
            dtatendimento: dtatendimento,
            consultor: consultor,
            valorauto: valorauto,
            valorimo: valorimo,
            cpff: cpff,
            nomef: nomef,
            rgf: rgf,
            orgaoemissorf: orgaoemissorf,
            enderecof: enderecof,
            numerof: numerof,
            bairrof: bairrof,
            cidadef: cidadef,
            uff: uff,
            estadocivilf: estadocivilf,
            nomeconjugue: nomeconjugue,
            cpfconjugue: cpfconjugue,
            profissaof: profissaof,
            cnascimentof: cnascimentof,
            nacionalidadef: nacionalidadef,
            fonefixof: fonefixof,
            celularf: celularf,
            emailf: emailf,
            bancof: bancof,
            agenciaf: agenciaf,
            contaf: contaf,
            formapagamentof: formapagamentof,
            cepf: cepf,
            cnpjj: cnpjj,
            razaosocialj: razaosocialj,
            nomefantasiaj: nomefantasiaj,
            fundacaoj: fundacaoj,
            capitalsocialj: capitalsocialj,
            tipocapitalj: tipocapitalj,
            atividadej: atividadej,
            nomesocioj: nomesocioj,
            cargosocioj: cargosocioj,
            particsocioj: particsocioj,
            cepj: cepj,
            ruaj: ruaj,
            numeroj: numeroj,
            bairroj: bairroj,
            cidadej: cidadej,
            ufj: ufj,
            estadocivilj: estadocivilj,
            fonefixoj: fonefixoj,
            celularj: celularj,
            emailj: emailj,
            bancoj: bancoj,
            agenciaj: agenciaj,
            contaj: contaj,
            formapagamentoj: formapagamentoj,
            cpfsocioj: cpfsocioj,
            tiporadiopessoastring: tiporadiopessoa,
            autocheck: autocheck,
            imocheck: imocheck,
            tipocliente: tipocliente,
            dtnascimentoconjugue: dtnascimentoconjugue,
            dtnascimento: dtnascimento,
            complemento: complemento
        });

        dtcontatos = JSON.stringify(dtcontatos);
        
        $.ajax({

            type: 'post',
            cache: false,
            contentType: 'application/json; charset=utf-8',
            url: '/Agendamento/SalvarAgendamento',
            data: dtcontatos,
            success: function (json) {

                if (json == "-1")
                {
                    $(".modalbodymsg").html("<strong>Os campos, Nome, Data de Atendimento, Tipo e Consultor são obrigatórios</strong>");
                    $('.btnokmodal').attr('onclick', ' ');
                    $(".modalmsg").modal('show');
                }
                if (json == "1")
                {
                    $(".modalbodymsg").html("<strong>Salvo com sucesso!</strong>");
                    $('.btnokmodal').attr('onclick', 'window.location.href = "../Agendamento/Agendamento"');
                    $(".modalmsg").modal('show');
                    
                }
                if (json == "0") {
                    $(".modalbodymsg").html("<strong>Erro ao salvar o agendamento</strong>");
                    $('.btnokmodal').attr('onclick', ' ');
                    $(".modalmsg").modal('show');
                }

            }


        });
        

        
        
    });

    $('.btnovocontato').click(function () {
        
        aux++
        var text = "<div class='row'>" +
                       "<div class='col-md-4'>" +
                           "<label>Data "+aux+"</label>"+
                           "<input type='date' class='form-control novocontdata"+aux+"'>"+
                       "</div>" +
                       "<div class='col-md-2'>" +
                           "<label>Hora"+aux+"</label>" +
                               "<input type='time' class='form-control novohora" + aux + "'>" +
                       "</div>" +
                   "</div>" +
                   "<br />" +
                   "<div class='row'>" +
                      "<div class='col-md-10'>" +
                           "<label>Observação " + aux + "</label>" +
                           "<textarea class='form-control maxlength novocontobs" + aux + "' rows='10'></textarea>" +
                           "<span i='content-countdown' title='10'>10 Caracteres Restantes</span>"+
                      "</div>" +
                   "</div>"+
                   "<br />";


        $('.novocontato').append(text);
    });

    $(".checkautonovo").click(function () {
        var check = $(".checkautonovo").is(":checked");
        if (check == true)
        {
            $(".novovalauto").removeAttr("disabled");
        }
        if (check == false)
        {
            $(".novovalauto").val("");
            $(".novovalauto").attr("disabled",true);

        }
    });

    $(".checkimonovo").click(function () {
        var check = $(".checkimonovo").is(":checked");
        if (check == true) {
            $(".novovalimoo").removeAttr("disabled");
        }
        if (check == false)
        {
            $(".novovalimoo").val("");
            $(".novovalimoo").attr("disabled", true);

        }
    });

    

    $('.btnbuscargeral').click(function () {
        
        
        

        consultorex = $("#ComboConsultor").val();
        tipoex = $("#TipoCliente").val();
        clienteex = $(".buscarcliente").val();
        aex = "";
        dataex = $(".buscardata").val();
        datate = $(".buscardataate").val();
        fina = $(".checkBoxFin").is(":checked");

        vlcor = ValorOuZero($("#ClassificacaoV").val());
        vlini = ValorOuZero($("#VrMinimo").val());
        vlfin = ValorOuZero($("#VrMaximo").val());


        BuscaGeral();

       
    });

    function ValorOuZero(valor) {
        return valor == '' ? '0' : valor;
    }

    $(".btnexcluiragendbusca").click(function () {

        var idagend = $(this).attr("id");
        var tipo = $(this).attr("data-name");
        
        $.ajax({

            type: 'post',
            cache: false,
            contentType: 'application/json; charset=utf-8',
            url: '/Agendamento/ExcluirAgendamento',
            data: "{ 'id': '" + idagend + "'}",
            success: function (json) {

                if (json == "true") {

                    $(".msgexclusao").html("Atendimento excluído com sucesso!");
                    $(".modalmsgexcluirbusca").modal("hide");
                    $(".modalmsgexcluido").modal("show");
                    $(".modalmsgexcluido").on('hidden.bs.modal', function () {
                        if (tipo == "buscageral") {
                            BuscaGeral();
                        }
                    });
                } else {
                    $(".msgexclusao").html("Não foi possível excluir o atendimento");
                    $(".modalmsgexcluirbusca").modal("hide");
                    $(".modalmsgexcluido").modal("show");

                }
            }


        });
    });

    $(".btnexcluiragend").click(function () {

        var idagend = $(this).attr("id");
        var tipo = $(this).attr("data-name");

        $.ajax({

            type: 'post',
            cache: false,
            contentType: 'application/json; charset=utf-8',
            url: '/Agendamento/ExcluirAgendamento',
            data: "{ 'id': '" + idagend + "'}",
            success: function (json) {

                if (json == "true") {

                    $(".msgexclusao").html("Atendimento excluído com sucesso!");
                    $(".modalmsgexcluir").modal("hide");
                    $(".modalmsgexcluido").modal("show");
                    $(".modalmsgexcluido").on('hidden.bs.modal', function () {
                       if (tipo == "buscageral"){
                            BuscaGeral();
                       } else {
                           window.location.reload();
                       }
                    });
                } else {
                    $(".msgexclusao").html("Não foi possível excluir o atendimento");
                    $(".modalmsgexcluir").modal("hide");
                    $(".modalmsgexcluido").modal("show");
                    
                }
            }


        });
    });

   

})


function BuscaGeral() {
    $("#divloader").show();
    var clicou = 0;
    $.ajax({
        type: 'post',
        cache: false,
        contentType: 'application/json; charset=utf-8',
        url: '/Agendamento/BuscarAgendamento',
        data: "{ 'consultor': '" + consultorex + "', 'tipo' : '" + tipoex + "','cliente' : '" + clienteex + "','data' : '" + dataex + "','dataate' : '" + datate + "','pag' : '" + clicou + "','finalizado' : '" + fina + "','cor' : " + vlcor + ",'valini' : " + vlini + ",'valfim' : " + vlfin + " }",
        success: function (json) {
            $('#pagetgeral').attr('data-quantidade', json.quantidade);

            aex = "<br />" +
                "<table class='table table-hover '>" +
                "<thead>" +
                        "<tr>" +
                             "<th>Data e hora</th>     " +
                             "<th>Cliente</th>  " +
                             "<th>Telefone Fixo</th> " +
                             "<th>Celular</th> " +
                             "<th>Tipo</th>     " +
                             "<th>Consultor</th>" +
                             "<th>Detalhe</th>  " +
                             "<th>Excluir</th>  " +
                        "</tr>" +
                    "</thead>" +
                    "<tbody class='tbodygeral'>";


            var classeStyle = "";

            $("#dvTotal").html("Total: " + json.quantidade);

            $(json.dados).each(function () {


                switch (this.Classificacao) {
                    case 0: classeStyle = ""; break;
                    case 1: classeStyle = "success"; break;
                    case 2: classeStyle = "transicao"; break;
                    case 3: classeStyle = "danger"; break;
                    case 4: classeStyle = "info"; break;
                }

                var dtcontatos = (this.dtcontato).split("-");
                var error = "";
                if (this.fg_urgente == true) {
                    error = "<span style='color: red'> - URGENTE</span>";
                }
                else {
                    error = "";
                }


                if (dtcontatos[0] == "") {
                    aex += "<tr class='" + classeStyle + "'>" +
                   "<td>" + dtcontatos[0] + this.hora + "</td>" +
                   "<td>" + this.nome + error + "</td>" +
                   "<td>" + this.fone1 + "</td>" +
                   "<td>" + this.fone2 + "</td>" +
                   "<td>" + this.tipocliente + "</td>" +
                   "<td>" + this.consultornome + "</td>" +
                   "<td>" +
                       "<button class='btn btn-info btn-circle btneditdetalhegeral' type='button' id='" + this.agendamentoid + "'>" +
                       "<i class='glyphicon glyphicon-list'></i>" +
                        "</button>" +
                   "</td>" +
                   "<td>" +
                       "<button class='btn btn-danger btn-circle btnexcluidetalhegeral' type='button' id='" + this.agendamentoid + "'>" +
                       "<i class='glyphicon glyphicon-remove'></i>" +
                        "</button>" +
                   "</td>" +
               "</tr>";
                } else {
                    aex += "<tr class='" + classeStyle + "'>" +
                   "<td>" + dtcontatos[2] + "/" + dtcontatos[1] + "/" + dtcontatos[0] + " " + this.hora + "</td>" +
                   "<td>" + this.nome + error + "</td>" +
                   "<td>" + this.fone1 + "</td>" +
                   "<td>" + this.fone2 + "</td>" +
                   "<td>" + this.tipocliente + "</td>" +
                   "<td>" + this.consultornome + "</td>" +
                   "<td>" +
                       "<button class='btn btn-info btn-circle btneditdetalhegeral' type='button' id='" + this.agendamentoid + "'>" +
                       "<i class='glyphicon glyphicon-list'></i>" +
                        "</button>" +
                   "</td>" +
                   "<td>" +
                       "<button class='btn btn-danger btn-circle btnexcluidetalhegeral' type='button' id='" + this.agendamentoid + "'>" +
                       "<i class='glyphicon glyphicon-remove'></i>" +
                        "</button>" +
                   "</td>" +
               "</tr>";
                }
                

            });

            aex += "</tbody>" +
                "</table>";

            $("#ComboConsultor").val(json.consultor);
            $("#TipoCliente").val(json.tipo);
            $(".buscarcliente").val(json.cliente);
            $(".buscardata").val(json.data);
            $(".buscardataate").val(json.dataate);
            $("#pagetgeral").show();

            $(".tablegeral").html(aex);

            $(".btneditdetalhegeral").click(function () {
                var id = $(this).attr("id");
                var href = "../Agendamento/DetalheAgendamento?url=" + id + "";
                window.open(href, '_blank');
                return false;
            });

            $('.btnexcluidetalhegeral').click(function () {

                $(".modalmsgexcluirbusca").modal('show');
                var id = $(this).attr('id');
                $('.btnexcluiragendbusca').attr('id', id);
                $('.btnexcluiragendbusca').attr('data-name', "buscageral");
            });
            
            var valgeral = $("#pagetgeral").attr('data-quantidade');
            var paggeral = Math.ceil(valgeral / 15) <= 1 ? $('#pagetgeral').hide() : Math.ceil(valgeral / 15);
            
            $('#pagetgeral').bootpag({
                total: paggeral,
                page: 1,
                maxVisible: 3,
                leaps: true,
                firstLastUse: true,
                first: '←',
                last: '→',
                wrapClass: 'pagination',
                activeClass: 'active',
                disabledClass: 'disabled',
                nextClass: 'next',
                prevClass: 'prev',
                lastClass: 'last',
                firstClass: 'first'
            }).on('page', function (event, num) {

                $.ajax({

                    type: 'post',
                    cache: false,
                    contentType: 'application/json; charset=utf-8',
                    url: '/Agendamento/BuscarAgendamento',
                    data: "{ 'consultor': '" + consultorex + "', 'tipo' : '" + tipoex + "','cliente' : '" + clienteex + "','data' : '" + dataex + "','dataate' : '" + datate + "','pag' : '" + num + "','finalizado' : '" + fina + "','cor' : " + vlcor + ",'valini' : " + vlini + ",'valfim' : " + vlfin + " }",
                    success: function (json) {
                        var text = "";
                        $(json.dados).each(function () {
                            
                            classeStyle = "";

                            switch (this.Classificacao) {
                                case 0: classeStyle = ""; break;
                                case 1: classeStyle = "success"; break;
                                case 2: classeStyle = "transicao"; break;
                                case 3: classeStyle = "danger"; break;
                                case 4: classeStyle = "info"; break;
                            }

                            var dtcontatos = (this.dtcontato).split("-");
                          
                            if (dtcontatos[0] == "") {
                                text += "<tr class='" + classeStyle + "'>" +
                               "<td>" + dtcontatos[0] + this.hora + "</td>" +
                               "<td>" + this.nome + "</td>" +
                               "<td>" + this.fone1 + "</td>" +
                               "<td>" + this.fone2 + "</td>" +
                               "<td>" + this.tipocliente + "</td>" +
                               "<td>" + this.consultornome + "</td>" +
                               "<td>" +
                                   "<button class='btn btn-info btn-circle btneditdetalhegeral' type='button' id='" + this.agendamentoid + "'>" +
                                   "<i class='glyphicon glyphicon-list'></i>" +
                                    "</button>" +
                               "</td>" +
                               "<td>" +
                                   "<button class='btn btn-danger btn-circle btnexcluidetalhegeral' type='button' id='" + this.agendamentoid + "'>" +
                                   "<i class='glyphicon glyphicon-remove'></i>" +
                                    "</button>" +
                               "</td>" +
                           "</tr>";
                            } else {
                                text += "<tr class='" + classeStyle + "'>" +
                               "<td>" + dtcontatos[2] + "/" + dtcontatos[1] + "/" + dtcontatos[0] + " " + this.hora + "</td>" +
                               "<td>" + this.nome + "</td>" +
                               "<td>" + this.fone1 + "</td>" +
                               "<td>" + this.fone2 + "</td>" +
                               "<td>" + this.tipocliente + "</td>" +
                               "<td>" + this.consultornome + "</td>" +
                               "<td>" +
                                   "<button class='btn btn-info btn-circle btneditdetalhegeral' type='button' id='" + this.agendamentoid + "'>" +
                                   "<i class='glyphicon glyphicon-list'></i>" +
                                    "</button>" +
                               "</td>" +
                               "<td>" +
                                   "<button class='btn btn-danger btn-circle btnexcluidetalhegeral' type='button' id='" + this.agendamentoid + "'>" +
                                   "<i class='glyphicon glyphicon-remove'></i>" +
                                    "</button>" +
                               "</td>" +
                           "</tr>";
                            }

                        });

                        $(".tbodygeral").html(text);

                        $(".btneditdetalhegeral").click(function () {
                            var id = $(this).attr("id");
                            var href = "../Agendamento/DetalheAgendamento?url=" + id + "";
                            window.open(href, '_blank');
                            return false;
                        });

                        $('.btnexcluidetalhegeral').click(function () {

                            $(".modalmsgexcluirbusca").modal('show');
                            var id = $(this).attr('id');
                            $('.btnexcluiragendbusca').attr('id', id);
                            $('.btnexcluiragendbusca').attr('data-name', "buscageral");
                        });

                    
                }

                })
        })
        }
            
    });



}

function Detalhe(idagend) {

    var data = { id: idagend };
    var nome = "";
    data = JSON.stringify(data);

    $.ajax({
        type: 'post',
        cache: false,
        contentType: 'application/json; charset=utf-8',
        url: '/Agendamento/DetalheAgendamento',
        data: data,
        beforeSend: function () {
            location.href = "../Agendamento/NovoAgendamento";
        },
        success: function (json) {
            
            nome = json.nome;

        },
        error: function (textStatus, errorThrown) {
            alert("Erro");
        },
        complete: function () {
            $("#nomecliente").val(nome);
        }

    });

}