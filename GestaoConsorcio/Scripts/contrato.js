﻿var filtro = {
    "nrocontr": null,
    "situacao": null,
    "datainicial": null,
    "datafinal": null,
    "pag": 0
};

$(function () {

   
    $(".btndetalhecontrato").click(function () {
        
        var id = $(this).attr('id');

        $.ajax({

            type: 'post',
            cache: false,
            contentType: 'application/json; charset=utf-8',
            url: '/Contrato/DetalheContrato',
            data: "{ 'id': '" + id + "'}",
            success: function (json) {

                $(json).each(function () {

                    if (this.fg_Hidden == "hidden") {
                        $('.fg_erro').hide();
                        $('.fg_cancel').hide();
                        $('.fg_hidden').show();
                    } else {
                        $('.fg_erro').hide();
                        $('.fg_hidden').hide();
                        $('.fg_cancel').show();
                    }
                    
                    if (this.fg_Erro == "erro")
                    {
                        $('.fg_erro').show();
                        $('.fg_erro_msg').html("<h2>Contrato não processado!</h2>");
                        $('.fg_cancel').hide();
                        $('.fg_hidden').hide();
                    }
                    var DtNascimento = this.DtNascimento == null ? "" : this.DtNascimento;
                    var DtAdesao = this.DtAdesao == null ? "" : this.DtAdesao;
                    var DtAlocacao = this.DtAlocacao == null ? "" : this.DtAlocacao;
                    var DtCadastro = this.DtCadastro == null ? "" : this.DtCadastro;
                    var DtAssembleia = this.DtAssembleia == null ? "" : this.DtAssembleia;
                    var DtAssembleiaAtual = this.DtAssembleiaAtual == null ? "" : this.DtAssembleiaAtual;

                    $('.nome').html("Nome: " + this.nome);
                    $('.cota').html("Cota: " + this.cota);
                    $('.grupo').html("Grupo: " + this.grupo);
                    $('.tipocota').html("Tipo Cota: " + this.tipocota);
                    $('.dtnascimento').html("Dt. Nascimento: " + DtNascimento);
                    $('.contrato').html("Contrato: " + this.NumeroContrato);
                    $('.cpf').html("Cpf/Cnpj:" + this.cpf);
                    $('.endereco').html("Endereço:" + this.Endereco);
                    $('.DtAdesao').html("Dt. Adesão: " + DtAdesao);
                    $('.DtAlocacao').html("Dt.Alocação: " + DtAlocacao);
                    $('.sitcobranca').html("Sit. Cobrança: " + this.sitcobranca);
                    $('.TipoVenda').html("Tipo de Venda: " + this.TipoVenda);
                    $('.BemObjeto').html("Bem Objeto: " + this.BemObjeto);
                    $('.FilialAdm').html("Filial Adm: " + this.FilialAdm);
                    $('.PtoEntrega').html("Pto. Entrega: " + this.PtoEntrega);
                    $('.TaxaAdesao').html("Taxa Adesão: " + this.TaxaAdesao);
                    $('.Cont').html("Cont: " + this.Cont);
                    $('.DtCadastro').html("Dt. Cadastro: " + DtCadastro);
                    $('.DtAssembleia').html("1ª Assembleia: " + DtAssembleia);
                    $('.PlanoBasico').html("Plano Básico: " + this.PlanoBasico);
                    $('.PlanoVenda').html("Plano Venda: " + this.PlanoVenda);
                    $('.FilialVenda').html("Filial Venda: " + this.FilialVenda);
                    $('.PtoVenda').html("Pto. Venda: " + this.PtoVenda);
                    $('.TaxaAdm').html("Taxa Adm: " + this.TaxaAdm);
                    $('.FndReserva').html("Fnd. Reserva: " + this.FndReserva);
                    $('.Seguros').html("Seguros: " + this.Seguros);
                    $('.AssembleiaAtual').html("Parcela Atual: " + this.AssembleiaAtual);
                    $('.Vencto').html("Vencimento: " + this.Vencto);
                    $('.SaldoDevedor').html("Saldo Devedor: " + this.SaldoDevedor);
                    $('.Atraso').html("Atraso: " + this.Atraso);
                    $('.ValorParcela').html("Valor da Parcela: " + this.ValorParcela);
                    $('.DtAssembleiaAtual').html("Dt. Assembleia Atual: " + DtAssembleiaAtual);
                    $('.ValoresPgos').html("Valores Pagos: " + this.ValoresPgos);
                    $('.QtdPclsPagas').html("Qtd.Parcelas Pagas: " + this.QtdPclsPagas);



                });
            }


        });

    });


    $('.btnfecharmodal').click(function () {
       
            $('.fg_hidden').hide();
       
            $('.fg_cancel').hide();
        

    });


    $('.btnbuscargeralcontrato').click(function () {

        filtro.nrocontr = $("#vContrato").val();
        filtro.situacao = $("#Situacao").val();
        filtro.datainicial = $("#vDe").val();
        filtro.datafinal = $("#vAte").val();

        BuscaGeralContrato(filtro);

    });

})



function BuscaGeralContrato(dados) {

    $("#divloader").show();
    var clicou = 0;
    $.ajax({
        type: 'post',
        cache: false,
        contentType: 'application/json; charset=utf-8',
        url: '/contrato/FiltrarRelatorio',
        data: JSON.stringify(dados),
        success: function (retorno) {
            var json = JSON.parse(retorno);

            $('#pagetgeral').attr('data-quantidade', json.quantidade);

            aex = "<br />" +
                "<table class='table table-hover '>" +
                "<thead>" +
                        "<tr>" +
                             "<th>Data e hora</th>     " +
                             "<th>Contrato</th>  " +
                             "<th>Situação</th> " +
                             "<th>Tipo Venda</th> " +
                             "<th>Parcela1</th>     " +
                             "<th>Parcela2</th>" +
                             "<th>Parcela3</th>  " +
                             "<th>Parcela4</th>  " +
                        "</tr>" +
                    "</thead>" +
                    "<tbody class='tbodygeral'>";


            var classeStyle = "";

            //$("#dvTotal").html("Total: " + json.Qtde);

            $(json.Itens).each(function () {

                
                aex += "<tr>" +
                "<td>" + getFormattedDate(this.DataVenda) + "</td>" +
                "<td>" + TratarString(this.NumContrato) + "</td>" +
                "<td>" + TratarString(this.Situacao) + "</td>" +
                "<td>" + TratarString(this.TipoVenda) + "</td>" +
                "<td>" + getFormattedDate(this.Parcela1) + "</td>" +
                "<td>" + getFormattedDate(this.Parcela2) + "</td>" +
                "<td>" + getFormattedDate(this.Parcela3) + "</td>" +
                "<td>" + getFormattedDate(this.Parcela4) + "</td>" +
                "</tr>";


            });

            aex += "</tbody>" +
                "</table>";

            $(".tablegeral").html(aex);
        }

    });



}

function TratarString(parm) {
    if (parm == null) {
        return "";
    }
    else
    {
        return parm;
    }
}


function getFormattedDate(parm) {

    if (parm == null || parm == "") return "";

    var date = new Date(parm);
    var year = date.getFullYear();
    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return day + '/' + month + '/' + year;
}

