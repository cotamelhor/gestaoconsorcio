﻿
function Validacao() {
    alert("entrou validacao");

    $('form').validate({
        rules: {
            nomecliente: {
                required: true
            },
            novofone1: {
                required: true
            }
        },
        messages: {
            nomecliente: {
                required: "Por favor, digite o nome"
            },
            novofone1: {
                required: "Por favor, digite um telefone"
            }
        },
        highlight: function (element) {
            var id_attr = "#" + $(element).attr("id") + "1";
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            var id_attr = "#" + $(element).attr("id") + "1";
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.length) {
                error.insertAfter(element);
            } else {
                error.insertAfter(element);
            }
        }
    });

    }
