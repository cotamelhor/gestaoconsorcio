﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GestaoConsorcio.Startup))]
namespace GestaoConsorcio
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
