﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestaoConsorcio.Models.View
{
    public class AgendamentosLigacaoVM
    {
        public List<ConsultorVM> Consultor { get; set; }

        public int IdConsultor { get; set; }

        [Required(ErrorMessage = "Por favor, empreencher o campor Protocolo")]
        [Display(Name = "ProtocoloAtendimentoL")]
        public string ProtocoloAtendimentoL { get; set; }

        [Required(ErrorMessage = "Por favor, empreencher o campo Data")]
        [Display(Name = "DtContatoL")]
        public string DtContatoL { get; set; }

        [Display(Name = "ObservacaoL")]
        public string ObservacaoL { get; set; }

        [Required(ErrorMessage = "Por favor, empreencher o campo Hora")]
        [Display(Name = "HoraL")]
        public string HoraL { get; set; }

        [Required(ErrorMessage = "Por favor, empreencher o campo Nome")]
        [Display(Name = "NomeL")]
        public string NomeL { get; set; }

        [Display(Name = "FoneFixoL")]
        public string FoneFixoL { get; set; }

        [Display(Name = "FoneCelularL")]
        public string FoneCelularL { get; set; }

        public bool CheckUrgente { get; set; }

        public int? AgendamentoSituacaoFK { get; set; }
    }

}