﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestaoConsorcio.Models.View
{
    public class TipoConsorcioVM
    {

        public int tipoconsorcioid { get; set; }
        public string descricao { get; set; }


    }

    public class TipoRoles
    {
        public List<Tipo> BackOffice { get; set; }
    }

    public class Tipo
    {
        public string Simulacao { get; set; }
        public string Vendas    { get; set; }
        public string Ligacao   { get; set; }
        public string Solitacao { get; set; }
    }



}