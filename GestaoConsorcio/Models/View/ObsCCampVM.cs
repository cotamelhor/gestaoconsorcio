﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestaoConsorcio.Models.View
{
    public class ObsCCampVM
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public string Texto { get; set; }
        public string Consultor { get; set; }
    }
}