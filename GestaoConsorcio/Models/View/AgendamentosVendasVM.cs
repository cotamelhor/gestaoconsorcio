﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestaoConsorcio.Models.View
{
    public class AgendamentosVendasVM
    {
        public List<ConsultorVM> Consultor { get; set; }

        public int IdConsultor { get; set; }

        [Required(ErrorMessage = "Por favor, empreencher o campo Protocolo")]
        [Display(Name = "ProtocolocoAtendimentoV")]
        public string ProtocoloAtendimentoV { get; set; }

        [Required(ErrorMessage = "Por favor, empreencher o campo Data")]
        [Display(Name = "DtContatoV")]
        public string DtContatoV { get; set; }

        [Display(Name = "ObservacaoV")]
        public string ObservacaoV { get; set; }

        [Required(ErrorMessage = "Por favor, empreencher o campo Hora")]
        [Display(Name = "HoraV")]
        public string HoraV { get; set; }

        [Display(Name = "ClassificacaoV")]
        public int ClassificacaoV { get; set; }
    }

}