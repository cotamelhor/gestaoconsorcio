﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestaoConsorcio.Models.View
{
    public class CampanhaImportacaoVM
    {
        public int CampanhaID { get; set; }
        public int ComboConsultorID { get; set; }
        public string Texto { get; set; }
    }
}