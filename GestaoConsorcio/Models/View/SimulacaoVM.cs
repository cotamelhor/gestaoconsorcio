﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestaoConsorcio.Models.View
{

    public class SimulacaoVM
    {
        [Required(ErrorMessage = "Por favor, empreencher o campo Protocolo")]
        [Display(Name = "ProtocoloAtends")]
        public string ProtocoloAtends { get; set; }
       
        [Display(Name="Observacao")]
        public string Observacao { get; set; }

        [Required(ErrorMessage = "Por favor, empreencher o campo Email")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        public int Consultor { get; set; }
        
    }

}