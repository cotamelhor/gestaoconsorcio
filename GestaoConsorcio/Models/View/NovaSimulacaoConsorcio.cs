﻿using System.ComponentModel.DataAnnotations;

namespace GestaoConsorcio.Models.View
{
    public class NovaSimulacaoConsorcio : SimulacaoConsorcio
    {
        public NovaSimulacaoConsorcio(decimal renda, int numeroMaxCartas, bool menorParcela, bool menorLance, bool menorTempoContemplacao)
        {
            Renda = renda;
            NumeroMaxCartas = numeroMaxCartas;
            MenorParcela = menorParcela;
            MenorLance = menorLance;
            //MenorTempoContemplacao = menorTempoContemplacao;
        }

        [DataType(DataType.Currency)]
        public decimal Renda { get; set; }

        public int NumeroMaxCartas { get; set; }

        public bool MenorParcela { get; set; }

        public bool MenorLance { get; set; }

        //public bool MenorTempoContemplacao { get; set; }
    }
}