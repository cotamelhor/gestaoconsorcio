﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestaoConsorcio.Models.View
{
    public class AgendamentoHistoricoVM
    {
        public int Id { get; set; }
        public string Atendente { get; set; }
        public string Cliente { get; set; }
        public string CriadoEm { get; set; }
        public string TipoInicial { get; set; }
        public string TipoFinal { get; set; }
    }
}