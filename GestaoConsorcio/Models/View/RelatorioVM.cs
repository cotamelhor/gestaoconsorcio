﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace GestaoConsorcio.Models.View
{
    public class RelatorioVM
    {
        public int Qtde { get; set; }
        public List<ItemRelatorio> Itens { get; set; }
    }

    public class ItemRelatorio
    {
        public string NumContrato { get; set; }
        public string Situacao { get; set; }
        public string TipoVenda { get; set; }
        public DateTime DataVenda { get; set; }
        public DateTime? Parcela1 { get; set; }
        public DateTime? Parcela2 { get; set; }
        public DateTime? Parcela3 { get; set; }
        public DateTime? Parcela4 { get; set; }

        public override string ToString()
        {
            var formatCode = "yyyy-MM-dd HH:mm:ss";

            return "{NumContrato:" + NumContrato +
                    ", Situacao:'', TipoVenda:'', DataVenda:"+ DateTime.ParseExact(this.DataVenda.ToString(), formatCode, CultureInfo.InvariantCulture)
            ;
        }

    }
}