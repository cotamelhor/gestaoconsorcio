﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestaoConsorcio.Models.View
{
    public class AgendamentosVM
    {
        public List<ConsultorVM> Consultor { get; set; }
        public List<ContatosVM> contatos { get; set; }
        public List<DadosAgen> agendamento{ get; set; }
        public string fg_Hidden { get; set; }
        public string msg { get; set; }
        public int? Situacao { get; set; }
    }

    public class Retorno
    {
        public List<DadosAgen> dados { get; set; }
        public int quantidade { get; set; }
        public string consultor { get; set; }
        public string cliente { get; set; }
        public string tipo { get; set; }
        public string data { get; set; }
        public string dataate { get; set; }
    }

    public class TarefasBackOffice
    {
        public List<DadosAgen> tarefahoje { get; set; }
        public List<DadosAgen> solicitacao { get; set; }
    }

    public class DadosAgen
    {
        [Required(ErrorMessage = "Por favor, empreencher o número do protocolo")]
        [Display(Name = "ProtocolocoAtendimento")]
        public string ProtocolocoAtendimento { get; set; }
        public int qtdade { get; set; }
        public string complemento { get; set; }
        public string dtnascimento { get; set; }
        public string dtnascimentoconjugue { get; set; }
        public string hora { get; set; }
        public string UltimoContatoNome { get; set; }
        public int UltimoContato { get; set; }
        public string protocolo { get; set; }
        public string numcontrato { get; set; }
        public int agendamentoid { get; set; }
        public string nome                        {get;set;}
        public DateTime? dtexpedicao                 {get;set;}
        public string fone1                       {get;set;}
        public string fone2                       {get;set;}
        public string email                       {get;set;}
        public DateTime? dtatendimento               {get;set;}
        public int? consultor                   {get;set;}
        public string consultornome { get; set; }
        public string valorauto                   {get;set;}
        public string  valorimo                    {get;set;}
        public string cpff                        {get;set;}
        public string nomef                       {get;set;}
        public string rgf                         {get;set;}
        public string orgaoemissorf               {get;set;}
        public string enderecof                   {get;set;}
        public string numerof { get; set; }
        public string bairrof                     {get;set;}
        public string cidadef                     {get;set;}
        public string uff                         {get;set;}
        public int? estadocivilf                {get;set;}
        public int? AgendamentoSituacaoFK { get; set; }
        public string nomeconjugue                {get;set;}
        public string cpfconjugue                 {get;set;}
        public string profissaof                  {get;set;}
        public string cnascimentof                {get;set;}
        public string nacionalidadef              {get;set;}
        public string fonefixof                   {get;set;}
        public string celularf                    {get;set;}
        public string emailf                      {get;set;}
        public string bancof                      {get;set;}
        public string agenciaf                    {get;set;}
        public string contaf                      {get;set;}
        public int? formapagamentof             {get;set;}
        public string cepf                        {get;set;}
        public string cnpjj                       {get;set;}
        public string razaosocialj                {get;set;}
        public string nomefantasiaj               {get;set;}
        public string fundacaoj                   {get;set;}
        public string capitalsocialj              {get;set;}
        public string tipocapitalj                {get;set;}
        public string atividadej                  {get;set;}
        public string nomesocioj                  {get;set;}
        public string cargosocioj                 {get;set;}
        public string particsocioj                {get;set;}
        public string cepj                        {get;set;}
        public string ruaj                        {get;set;}
        public string numeroj { get; set; }
        public string bairroj                     {get;set;}
        public string cidadej                     {get;set;}
        public string ufj                         {get;set;}
        public string estadocivilj                {get;set;}
        public string fonefixoj                   {get;set;}
        public string celularj                    {get;set;}
        public string emailj                      {get;set;}
        public string bancoj                      {get;set;}
        public string agenciaj { get; set; }
        public string contaj                      {get;set;}
        public int? formapagamentoj             {get;set;}
        public string cpfsocioj                   {get;set;}
        public string dtcontato                   {get;set;}
        public DateTime dtcontatodate { get; set; }
        public string obscontato                  {get;set;}
        public bool? tiporadiopessoa             {get;set;}
        public string autocheck                   {get;set;}
        public string imocheck                    {get;set;}
        public string tipocliente { get; set; }
        public string dtexpedicaostring { get; set; }
        public string dtatendimentostring { get; set; }
        public string tiporadiopessoastring { get; set; }
        public List<ContatosVM> contatos { get; set; }
        public int? contato { get; set; }
        public bool? fg_urgente { get; set; }
        public bool? fg_finalizado { get; set; }
        public string Operacaof { get; set; }
        public string Operacaoj { get; set; }
        public int Classificacao { get; set; }
    }




}