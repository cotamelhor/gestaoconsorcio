﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GestaoConsorcio.Models.View
{
    public class SimulacaoConsorcio
    {

        public int IdSimulacao { get; set; }

        public int IdUsuario { get; set; }

        public string Nome { get; set; }

        [DataType(DataType.Currency)]
        public decimal ValorCarta { get; set; }

        public decimal Parcela { get; set; }

        //public int Contemplacao { get; set; }

        [DataType(DataType.Currency)]
        public decimal LanceProprio { get; set; }

        public DateTime CriadoEm { get; set; }
    }
}