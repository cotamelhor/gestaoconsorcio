﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestaoConsorcio.Models.View
{
    public class CampanhaVM
    {
        public int ContatoCampanhaID { get; set; }
        public string Nome { get; set; }
        public string Telefone { get; set; }
        public string ValoresCarta { get; set; }
        public string Estado { get; set; }
        public DateTime CriadoEm { get; set; }
        public DateTime? UltimaAcao { get; set; }
        public string UltimaAcaoDesc { get; set; }
    }
}