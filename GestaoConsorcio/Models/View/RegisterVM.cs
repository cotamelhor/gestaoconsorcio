﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestaoConsorcio.Models.View
{
    public class ConsultorVM
    {

        public int consultorid { get; set; }
        public string nome { get; set; }
        public string sobrenome { get; set; }

    }


    public class ContatosVM
    {
        public string hora { get; set; }
        public int agendamentoid { get; set; }
        public int contadoid { get; set; }
        public int contfirst { get; set; }
        public int contsec { get; set; }
        public string dtcontato { get; set; }
        public string observacao { get; set; }


    }

}