﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GestaoConsorcio.Models.View
{
    public class RetornoSimulacaoConsorcio
    {
        public RetornoSimulacaoConsorcio(decimal valorDoBem, decimal totalPagar, decimal lanceProprio, decimal taxaAdministrativa, float percentualTaxaAdministrativa, List<Parcelas> parcelasAntesContemplacao, List<Parcelas> parcelasAposContemplacao, double mediaContemplacao, double mediaNaoContemplado, DateTime proximaAssembleia, int previsaoContemplacao, int numeroParcelas)
        {
            ValorDoBem = valorDoBem;
            TotalPagar = totalPagar;
            LanceProprio = lanceProprio;
            TaxaAdministrativa = taxaAdministrativa;
            PercentualTaxaAdministrativa = percentualTaxaAdministrativa;
            ValorTaxaAdministrativa = totalPagar * (taxaAdministrativa / 100);
            ParcelasAntesContemplacao = parcelasAntesContemplacao;
            ParcelasAposContemplacao = parcelasAposContemplacao;
            MediaContemplacao = mediaContemplacao;
            MediaNaoContemplado = mediaNaoContemplado;
            ProximaAssembleia = proximaAssembleia;
            PrevisaoContemplacao = previsaoContemplacao;
            NumeroParcelas = numeroParcelas;
        }

        [DataType(DataType.Currency)]
        public decimal ValorDoBem { get; set; }

        [DataType(DataType.Currency)]
        public decimal TotalPagar { get; set; }

        [DataType(DataType.Currency)]
        public decimal LanceProprio { get; set; }

        [DataType(DataType.Currency)]
        public decimal TaxaAdministrativa { get; set; }

        public float PercentualTaxaAdministrativa { get; set; }

        [DataType(DataType.Currency)]
        public decimal ValorTaxaAdministrativa { get; set; }

        public List<Parcelas> ParcelasAntesContemplacao { get; set; }

        public List<Parcelas> ParcelasAposContemplacao { get; set; }

        public int NumeroParcelas { get; set; }

        public double MediaContemplacao { get; set; }

        public double MediaNaoContemplado { get; set; }

        public DateTime ProximaAssembleia { get; set; }

        public int PrevisaoContemplacao { get; set; }

    }

    public class Parcelas
    {
        public int ParcelaDe { get; set; }

        public int ParcelaAte { get; set; }

        [DataType(DataType.Currency)]
        public decimal ValorParcela { get; set; }
    }
}