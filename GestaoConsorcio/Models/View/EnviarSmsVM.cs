﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestaoConsorcio.Models.View
{
    public class EnviarSmsVM
    {
        public string Telefones { get; set; }
        public string Mensagem { get; set; }
        public string NumeroAtendimento { get; set; }
        public List<string> ListaTelefones {
            get
            {
                return Telefones.Split(',').ToList();
            }
        }
    }
}