﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestaoConsorcio.Models.View
{
    public class ChatVM
    {
        public string AgendamentoId { get; set; }
        public string Protocolo { get; set; }
        public string Cliente { get; set; }
        public string Sessao { get; set; }
        public string Resumo { get; set; }
        public string Duracao { get; set; }
        public string IP { get; set; }
        public List<ConsultaChatLib.Detalhe> Conversa { get; set; }

    }



}