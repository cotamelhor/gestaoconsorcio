﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestaoConsorcio.Models.View
{

    public class ContratoVM
    {
        [Required(ErrorMessage = "Por favor, empreencher o campo Protocolo")]
        [Display(Name = "ProtocoloAtends")]
        public string ProtocoloAtends { get; set; }
        public int id { get; set; }
        
        [Required(ErrorMessage = "Por favor, empreencher o campo Número do Contrato")]
        [RegularExpression("^[0-9]", ErrorMessage = "Por favor, digite apenas números no número do contrato")]
        [Display(Name = "NumeroContrato")]
        public string NumeroContrato { get; set; }

        [Display(Name="Observacao")]
        public string Observacao { get; set; }

        [Required(ErrorMessage = "Por favor, empreencher o campo Email")]
        [Display(Name = "Email")]
        public string Email { get; set; }
        public string Endereco { get; set; }
        public string grupo { get; set; }
        public string cota { get; set; }
        public string tipocota { get; set; }
        public string CheckBoxAlocado { get; set; }
        public string ConsultorID { get; set; }
        public string nome { get; set; }
        public string cpf { get; set; }
        public string DtNascimento { get; set; }
        public string situacao { get; set; }
        public string DtAdesao { get; set; }
        public string DtCadastro { get; set; }
        public string DtAlocacao { get; set; }
        public string DtAssembleia { get; set; }
        public string sitcobranca { get; set; }
        public string PlanoBasico { get; set; }
        public string TipoVenda { get; set; }
        public string PlanoVenda { get; set; }
        public string BemObjeto { get; set; }
        public string FilialVenda { get; set; }
        public string FilialAdm { get; set; }
        public string PtoVenda { get; set; }
        public string PtoEntrega { get; set; }
        public string TaxaAdm { get; set; }
        public string TaxaAdesao { get; set; }
        public string FndReserva { get; set; }
        public string Cont { get; set; }
        public string Seguros { get; set; }
        public string DtAssembleiaAtual { get; set; }
        public string AssembleiaAtual { get; set; }
        public string Vencto { get; set; }
        public string ValoresPgos { get; set; }
        public string SaldoDevedor { get; set; }
        public string Atraso { get; set; }
        public string ValorParcela { get; set; }
        public string NumeroParcel { get; set; }
        public string QtdPclsPagas { get; set; }
        public string fg_Pagina { get; set; }
        public string fg_Principal { get; set; }
        public string fg_Detalhado { get; set; }
        public string fg_Hidden { get; set; }
        public string fg_HiddenVenda { get; set; }
        public string fg_HiddenSaldoAcumulado { get; set; }
        public string fg_HiddenErro { get; set; }
        public string MsgErro { get; set; }
        public string fg_Erro { get; set; }
        public List<Contrato> contratos { get; set; }
        public List<ConsultorVM> consultores { get; set; }
        public DateTime? InicioFiltro { get; set; }
        public DateTime? FimFiltro { get; set; }

    }

    public class Contrato
    {
        public string numero { get; set; }
        public string protocoloatend { get; set; }
    }

    public class Consultores
    {
        public string consultorid { get; set; }
        public string nome { get; set; }
    }

    public class ConsultVM
    {
        public List<Contrato> contratos { get; set; }
        public List<Consultores> consultores { get; set; }
        public bool role { get; set; }
    }

    public class SalvarContratoVM
    {
        [Required(ErrorMessage = "Por favor, empreencher o campo Protocolo")]
        [Display(Name = "ProtocoloAtends")]
        public string ProtocoloAtends { get; set; }
        public int id { get; set; }

        [Required(ErrorMessage = "Por favor, empreencher o campo Número do Contrato")]
        [Display(Name = "NumeroContrato")]
        public string NumeroContrato { get; set; }

        public string Email { get; set; }
        public string grupo { get; set; }
        public string cota { get; set; }
        public string tipocota { get; set; }
        public string CheckBoxAlocado { get; set; }
        public string ConsultorID { get; set; }
        public string nome { get; set; }
        public string cpf { get; set; }
        public string DtNascimento { get; set; }
        public string situacao { get; set; }
        public string DtAdesao { get; set; }
        public string DtCadastro { get; set; }
        public string DtAlocacao { get; set; }
        public string DtAssembleia { get; set; }
        public string sitcobranca { get; set; }
        public string PlanoBasico { get; set; }
        public string TipoVenda { get; set; }
        public string PlanoVenda { get; set; }
        public string BemObjeto { get; set; }
        public string FilialVenda { get; set; }
        public string FilialAdm { get; set; }
        public string PtoVenda { get; set; }
        public string PtoEntrega { get; set; }
        public string TaxaAdm { get; set; }
        public string TaxaAdesao { get; set; }
        public string FndReserva { get; set; }
        public string Cont { get; set; }
        public string Seguros { get; set; }
        public string DtAssembleiaAtual { get; set; }
        public string AssembleiaAtual { get; set; }
        public string Vencto { get; set; }
        public string ValoresPgos { get; set; }
        public string SaldoDevedor { get; set; }
        public string Atraso { get; set; }
        public string ValorParcela { get; set; }
        public string NumeroParcel { get; set; }
        public string QtdPclsPagas { get; set; }
        public string fg_Pagina { get; set; }
        public string fg_Principal { get; set; }
        public string fg_Detalhado { get; set; }
        public string fg_Hidden { get; set; }
        public string fg_HiddenVenda { get; set; }
        public string fg_HiddenSaldoAcumulado { get; set; }
        public string fg_HiddenErro { get; set; }
        public string MsgErro { get; set; }
        public string fg_Erro { get; set; }
        public List<Contrato> contratos { get; set; }
        public List<ConsultorVM> consultores { get; set; }


    }
}