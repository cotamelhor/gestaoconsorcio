﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestaoConsorcio.Models.View
{

    public class ComissaoVM
    {
        public List<ConsultorVM> consultor { get; set; }

        public int ComboConsultor { get; set; }

        [Required(ErrorMessage="Por favor, digite o campo Comissão")]
        [Display(Name="Valor")]
        public string Valor { get; set; }
    }

}