﻿using System.ComponentModel.DataAnnotations;

namespace GestaoConsorcio.Models.View
{
    public class Financiamento
    {
        public Financiamento(decimal valorDoBem, decimal valorEntrada, decimal valorFinanciado, decimal valorTotal, decimal valorPrimeiraParcela, decimal valorUltimaParcela, int parcelas, float jurosAnual)
        {
            ValorDoBem = valorDoBem;
            ValorEntrada = valorEntrada;
            ValorFinanciado = valorFinanciado;
            ValorTotal = valorTotal;
            ValorPrimeiraParcela = valorPrimeiraParcela;
            ValorUltimaParcela = valorUltimaParcela;
            Parcelas = parcelas;
            JurosAnual = jurosAnual;
        }

        [DataType(DataType.Currency)]
        public decimal ValorDoBem { get; set; }

        [DataType(DataType.Currency)]
        public decimal ValorEntrada { get; set; }

        [DataType(DataType.Currency)]
        public decimal ValorFinanciado { get; set; }

        [DataType(DataType.Currency)]
        public decimal ValorTotal { get; set; }

        [DataType(DataType.Currency)]
        public decimal ValorPrimeiraParcela { get; set; }

        [DataType(DataType.Currency)]
        public decimal ValorUltimaParcela { get; set; }

        public int Parcelas { get; set; }

        public float JurosAnual { get; set; }
    }
}