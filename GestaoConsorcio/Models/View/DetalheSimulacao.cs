﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestaoConsorcio.Models.View
{
    public class DetalheSimulacao
    {
        public DetalheSimulacao(List<RetornoSimulacaoConsorcio> simulacao, Financiamento financiamento)
        {
            Simulacao = simulacao;
            Financiamento = financiamento;
        }

        public List<RetornoSimulacaoConsorcio> Simulacao { get; set; }

        public Financiamento Financiamento { get; set; }
    }
}