﻿using GestaoConsorcio.Models.Banco;
using GestaoConsorcio.Models.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestaoConsorcio.Models.Repositorio
{
    public class ComunicacaoBD
    {

        private db_gestaocotamelhor_desenvEntities banco = new db_gestaocotamelhor_desenvEntities();

        public string SalvarLogSms(int usuid, EnviarSmsVM model)
        {
            int numero = 0;
            try
            {
                banco.LogSms.Add(new LogSm()
                {
                    DtEnvio = new DateTime(),
                    Msg = model.Mensagem,
                    Telefone = model.Telefones,
                    Ligacao = string.IsNullOrEmpty(model.NumeroAtendimento.Trim()) || int.TryParse(model.NumeroAtendimento, out numero) ? 0 : int.Parse(model.NumeroAtendimento),
                    UsuarioFK = usuid
                });
                banco.SaveChanges();

                return "0";
            }
            catch
            {
                return "-1";
            }
        }

    }
}