﻿using GestaoConsorcio.Models.Banco;
using GestaoConsorcio.Models.View;
using GestaoConsorcio.Views.Agendamento;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace GestaoConsorcio.Models.Repositorio
{
    public class AgendamentoBD
    {
        private db_gestaocotamelhor_desenvEntities banco = new db_gestaocotamelhor_desenvEntities();

        public bool SalvarAgendamentoLigacao(AgendamentosLigacaoVM dados)
        {
            try
            {
                Agendamento ag = new Agendamento();

                ag.fg_Urgente = dados.CheckUrgente;
                ag.Nome = dados.NomeL;
                ag.Fone1 = dados.FoneFixoL;
                ag.Fone2 = dados.FoneCelularL;
                ag.ConsultorId = dados.IdConsultor;
                ag.Protocolo = dados.ProtocoloAtendimentoL;
                ag.fg_Ativo = "1";
                ag.Tipo = "L";
                ag.DtAtendimento = DateTime.Parse(DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day);
                var id = banco.Agendamentoes.Add(ag);

                Contato ct = new Contato();

                ct.AgendamentoID = id.AgendamentoId;
                ct.DtContato = DateTime.Parse(dados.DtContatoL);
                ct.Hora = dados.HoraL;
                ct.Observacao = dados.ObservacaoL;

                banco.Contatos.Add(ct);
                banco.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool SalvarAgendamentoVendas(AgendamentosVendasVM dados)
        {
            try
            {
                Agendamento ag = new Agendamento();

                ag.ConsultorId = dados.IdConsultor;
                ag.Protocolo = dados.ProtocoloAtendimentoV;
                ag.Classificacao = dados.ClassificacaoV;
                ag.fg_Ativo = "1";
                ag.Tipo = "C";
                ag.fg_Urgente = false;
                ag.DtAtendimento = DateTime.Parse(DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day);
                var id = banco.Agendamentoes.Add(ag);

                Contato ct = new Contato();

                ct.AgendamentoID = id.AgendamentoId;
                ct.DtContato = DateTime.Parse(dados.DtContatoV);
                ct.Hora = dados.HoraV;
                ct.Observacao = dados.ObservacaoV;

                banco.Contatos.Add(ct);
                banco.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public string BuscaProtocolo(string agendamento)
        {
            int agendID = Int32.Parse(agendamento);
            return banco.Agendamentoes.Where(x => x.AgendamentoId == agendID).FirstOrDefault().Protocolo.ToString();
        }

        public string ExcluirAgendamentoBD(int id)
        {
            var retorno = banco.Agendamentoes.Where(x => x.AgendamentoId == id).FirstOrDefault();
            string msg = "";
            try
            {
                retorno.fg_Ativo = "0";
                banco.SaveChanges();
                msg = "true";
            }
            catch
            {
                msg = "false";
            }

            return msg;
        }

        public string FinalizarAgendamentoBD(int id, string status, bool fgFinalizado = true)
        {
            var retorno = banco.Agendamentoes.Where(x => x.AgendamentoId == id).FirstOrDefault();
            string msg = "";
            try
            {
                retorno.fg_Finalizado = fgFinalizado;

                var busca = banco.Finalizadoes.Where(x => x.AgendamentoId == retorno.AgendamentoId).FirstOrDefault();

                if (busca != null)
                {
                    busca.Status = status;
                    busca.Dt_Finalizado = DateTime.Now;
                }
                else
                {
                    Finalizado fin = new Finalizado();
                    fin.Dt_Finalizado = DateTime.Now;
                    fin.AgendamentoId = retorno.AgendamentoId;
                    fin.Status = status;
                    banco.Finalizadoes.Add(fin);
                }
                banco.SaveChanges();
                msg = "true";
            }
            catch
            {
                msg = "false";
            }

            return msg;
        }

        public string SalvarAgendamentoBD(AgendamentosVM dados)
        {
            Agendamento agendamento = new Agendamento();

            try
            {
                if (dados.agendamento[0].tiporadiopessoastring == "pfisica")
                {
                    agendamento.Protocolo = dados.agendamento[0].protocolo;
                    if (dados.agendamento[0].numcontrato != null)
                    {
                        agendamento.NumContrato = Int32.Parse(dados.agendamento[0].numcontrato);
                    }

                    agendamento.Nome = dados.agendamento[0].nome;
                    agendamento.Fone1 = dados.agendamento[0].fone1;
                    agendamento.Fone2 = dados.agendamento[0].fone2;
                    agendamento.Email = dados.agendamento[0].email;
                    agendamento.DtAtendimento = dados.agendamento[0].dtatendimento;
                    agendamento.ConsultorId = dados.agendamento[0].consultor;
                    agendamento.Auto = bool.Parse(dados.agendamento[0].autocheck);
                    agendamento.DtNascimento = dados.agendamento[0].dtnascimento;
                    if (dados.agendamento[0].dtexpedicao != null)
                    {
                        agendamento.DataExpedicao = dados.agendamento[0].dtexpedicao;
                    }

                    agendamento.Complemento = dados.agendamento[0].complemento;
                    agendamento.Imobiliario = bool.Parse(dados.agendamento[0].imocheck);
                    agendamento.Cpf_Cnpj = dados.agendamento[0].cpff;
                    agendamento.RgNro = dados.agendamento[0].rgf;
                    agendamento.RgEmissor = dados.agendamento[0].orgaoemissorf;
                    agendamento.Logradouro = dados.agendamento[0].enderecof;
                    agendamento.Bairro = dados.agendamento[0].bairrof;
                    agendamento.Cidade = dados.agendamento[0].cidadef;
                    agendamento.Uf = dados.agendamento[0].uff;
                    if (dados.agendamento[0].estadocivilf != null && dados.agendamento[0].estadocivilf != 0)
                    {
                        agendamento.EstadoCivilFk = dados.agendamento[0].estadocivilf;
                    }
                    agendamento.NomeConjuge = dados.agendamento[0].nomeconjugue;
                    agendamento.CpfConjuge = dados.agendamento[0].cpfconjugue;
                    agendamento.DtNascimentoConjugue = dados.agendamento[0].dtnascimentoconjugue;
                    agendamento.Profissao = dados.agendamento[0].profissaof;
                    agendamento.CidNascimento = dados.agendamento[0].cnascimentof;
                    agendamento.Nacionalidade = dados.agendamento[0].nacionalidadef;
                    agendamento.FoneFixo = dados.agendamento[0].fonefixof;
                    agendamento.Celular = dados.agendamento[0].celularf;
                    agendamento.Banco = dados.agendamento[0].bancof;
                    agendamento.Agencia = dados.agendamento[0].agenciaf;
                    agendamento.Conta = dados.agendamento[0].contaf;
                    agendamento.FormaDePagamentoFk = dados.agendamento[0].formapagamentof;
                    agendamento.PessoaFisica = true;
                    agendamento.CEP = dados.agendamento[0].cepf;
                    if (dados.agendamento[0].valorauto != null)
                    {
                        agendamento.ValorAuto = dados.agendamento[0].valorauto;
                    }
                    if (dados.agendamento[0].valorimo != null)
                    {
                        agendamento.ValorImo = dados.agendamento[0].valorimo;
                    }
                }
                else if (dados.agendamento[0].tiporadiopessoastring == "pjuridica")
                {
                    agendamento.Protocolo = dados.agendamento[0].protocolo;
                    if (dados.agendamento[0].numcontrato != null)
                    {
                        agendamento.NumContrato = Int32.Parse(dados.agendamento[0].numcontrato);
                    }
                    agendamento.Complemento = dados.agendamento[0].complemento;
                    agendamento.PessoaFisica = false;
                    agendamento.Nome = dados.agendamento[0].nome;
                    agendamento.Fone1 = dados.agendamento[0].fone1;
                    agendamento.Fone2 = dados.agendamento[0].fone2;
                    agendamento.Email = dados.agendamento[0].email;
                    agendamento.DtAtendimento = dados.agendamento[0].dtatendimento;
                    agendamento.ConsultorId = dados.agendamento[0].consultor;
                    agendamento.Auto = bool.Parse(dados.agendamento[0].autocheck);
                    agendamento.Imobiliario = bool.Parse(dados.agendamento[0].imocheck);
                    agendamento.DtNascimento = dados.agendamento[0].dtnascimento;

                    if (dados.agendamento[0].valorauto != null)
                    {
                        agendamento.ValorAuto = dados.agendamento[0].valorauto;
                    }
                    if (dados.agendamento[0].valorimo != null)
                    {
                        agendamento.ValorImo = dados.agendamento[0].valorimo;
                    }

                    agendamento.Cpf_Cnpj = dados.agendamento[0].cnpjj;
                    agendamento.RazaoSocial = dados.agendamento[0].razaosocialj;
                    agendamento.NomeFantasia = dados.agendamento[0].nomefantasiaj;
                    agendamento.Fundacao = dados.agendamento[0].fundacaoj;
                    agendamento.CapitalSocial = dados.agendamento[0].capitalsocialj;
                    agendamento.TipoCapital = dados.agendamento[0].tipocapitalj;
                    agendamento.Atividade = dados.agendamento[0].atividadej;
                    agendamento.NomeSocio = dados.agendamento[0].nomesocioj;
                    agendamento.CargoSocio = dados.agendamento[0].cargosocioj;
                    agendamento.CPFSocio = dados.agendamento[0].cpfsocioj;
                    agendamento.ParticipacaoSocio = dados.agendamento[0].particsocioj;
                    agendamento.CEP = dados.agendamento[0].cepj;
                    agendamento.Logradouro = dados.agendamento[0].ruaj;
                    agendamento.Bairro = dados.agendamento[0].bairroj;
                    agendamento.Cidade = dados.agendamento[0].cidadej;
                    agendamento.Uf = dados.agendamento[0].ufj;
                    agendamento.FoneFixo = dados.agendamento[0].fonefixoj;
                    agendamento.Celular = dados.agendamento[0].celularj;
                    agendamento.Email = dados.agendamento[0].emailj;
                    agendamento.Banco = dados.agendamento[0].bancoj;
                    agendamento.Agencia = dados.agendamento[0].agenciaj;
                    agendamento.Conta = dados.agendamento[0].contaj;
                    agendamento.FormaDePagamentoFk = dados.agendamento[0].formapagamentoj;
                    agendamento.Tipo = dados.agendamento[0].tipocliente;
                }
                agendamento.fg_Ativo = "1";
                var idagend = banco.Agendamentoes.Add(agendamento);
                banco.SaveChanges();

                Contato cont = null;

                if (dados.contatos != null)
                {
                    int quantcontatos = dados.contatos.Count();

                    for (int i = 0; i < quantcontatos; i++)
                    {
                        if (dados.contatos[i].dtcontato != "" && dados.contatos[i].dtcontato != null)
                        {
                            cont = new Contato();
                            cont.Hora = dados.contatos[i].hora;
                            cont.DtContato = DateTime.Parse(dados.contatos[i].dtcontato);
                            cont.Observacao = dados.contatos[i].observacao;
                            cont.AgendamentoID = idagend.AgendamentoId;
                            var idcont = banco.Contatos.Add(cont);
                            banco.SaveChanges();
                            agendamento.UltimoContato = idcont.ContatoID;
                        }
                    }

                    banco.SaveChanges();
                }
                return "1";
            }
            catch
            {
                return "0";
            }
        }

        public List<DadosAgen> TarefaHojeBD(string id, Tipo t)
        {
            string dtnow = DateTime.Now.Year + "-" + (DateTime.Now.Month < 10 ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString()) + "-" + (DateTime.Now.Day < 10 ? "0" + DateTime.Now.Day.ToString() : DateTime.Now.Day.ToString());
            List<DadosAgen> tarefa = null;
            DateTime dnow = DateTime.Parse(dtnow);
            if (id == "")
            {
                tarefa = banco.vw_tarefas.Where(a => (a.fg_finalizado == false || a.fg_finalizado == null)).Select(s => new DadosAgen
                {
                    agendamentoid = s.agendamentoid,
                    consultornome = s.nomeconsultor + " " + s.sobrenomeconsultor,
                    hora = s.Hora == null ? "" : s.Hora,
                    nome = s.nome == null || s.nome == "" ? "" : s.nome.ToUpper(),
                    dtcontato = s.dtcontato.ToString(),
                    fone1 = s.fone1 == null ? "" : s.fone1,
                    fone2 = s.fone2 == null ? "" : s.fone2,
                    tipocliente = (s.tipo == "C" ? "Venda" : s.tipo == "S" ? "Solicitação" : s.tipo == "I" ? "Simulação" : "Ligação"),
                    fg_urgente = s.fg_urgente
                }).ToList();
            }
            else
            {
                int idcon = Int32.Parse(id);

                tarefa = banco.vw_tarefas.Where(a => (a.fg_finalizado == false || a.fg_finalizado == null) &&
                                                        a.ConsultorId == idcon).Select(s => new DadosAgen
                                                        {
                                                            agendamentoid = s.agendamentoid,
                                                            consultornome = s.nomeconsultor + " " + s.sobrenomeconsultor,
                                                            hora = s.Hora == null ? "" : s.Hora,
                                                            nome = s.nome == null || s.nome == "" ? "" : s.nome.ToUpper(),
                                                            dtcontato = s.dtcontato.ToString(),
                                                            fone1 = s.fone1 == null ? "" : s.fone1,
                                                            fone2 = s.fone2 == null ? "" : s.fone2,
                                                            tipocliente = (s.tipo == "C" ? "Venda" : s.tipo == "S" ? "Solicitação" : s.tipo == "I" ? "Simulação" : "Ligação"),
                                                            fg_urgente = s.fg_urgente
                                                        }).ToList();
            }
            return tarefa;
        }

        public List<DadosAgen> VerificarAgendamento(int cons)
        {
            DateTime dtde = DateTime.Now;
            DateTime dtate = DateTime.Now.AddMinutes(2);

            var busca = banco.vw_tarefas.Where(x => (x.dtcompleta >= dtde &&
                                                        x.dtcompleta <= dtate ||
                                                        x.dtcompleta >= DbFunctions.AddMinutes(dtde, -10) &&
                                                        x.dtcompleta <= DbFunctions.AddMinutes(dtate, -10) ||
                                                        x.dtcompleta >= DbFunctions.AddMinutes(dtde, -20) &&
                                                        x.dtcompleta <= DbFunctions.AddMinutes(dtate, -20) ||
                                                        x.dtcompleta >= DbFunctions.AddMinutes(dtde, -30) &&
                                                        x.dtcompleta <= DbFunctions.AddMinutes(dtate, -30)
                                                    ) &&
                                                    x.fg_finalizado == false &&
                                                    x.tipo == "L" &&
                                                    x.ConsultorId == cons).Select(s => new DadosAgen
                                                    {
                                                        agendamentoid = s.agendamentoid,
                                                        fg_urgente = s.fg_urgente,
                                                        nome = s.nome,
                                                        fone1 = s.fone1 == null ? "" : s.fone1,
                                                        fone2 = s.fone2 == null ? "" : s.fone2,
                                                        dtcontato = s.dtcompleta == null ? null : s.dtcompleta.ToString(),
                                                        tipocliente = (s.tipo == "C" ? "Venda" : s.tipo == "S" ? "Solicitação" : s.tipo == "I" ? "Simulação" : "Ligação"),
                                                        Classificacao = s.Classificacao
                                                    }).OrderByDescending(x => x.dtcontato).ToList();

            return busca;
        }

        public List<DadosAgen> BuscarAgendamentoBD(string consultor, string tipo, string cliente, string data, string dataate, bool finalizado, int cor, int valini, int valfim)
        {
            int consul = 0;

            if (tipo == "0" || tipo == "undefined")
            {
                tipo = "";
            }

            DateTime? dtde = null;
            DateTime? dtate = null;

            if (data != "")
            {
                dtde = DateTime.Parse(data);
            }

            if (dataate != "")
            {
                dtate = DateTime.Parse(dataate);
            }

            if (consultor != "Selecione" && consultor != "")
            {
                consul = Int32.Parse(consultor);
                var busca = banco.vw_tarefas.Where(a => (a.fg_finalizado == finalizado) &&
                                       (a.Classificacao == cor || cor == 0) &&
                                       ((a.ValorAuto >= valini || valini == 0) && (a.ValorAuto <= valfim || valfim == 0) || (a.ValorImo >= valini || valini == 0) && (a.ValorImo <= valfim || valfim == 0)) &&
                                       (a.tipo == tipo || tipo == "") &&
                                       (a.ConsultorId == consul) &&
                                       (a.nome.Contains(cliente) || cliente == "") &&
                                       (a.dtcontato >= dtde && a.dtcontato <= dtate || data == "" || dataate == "")).OrderByDescending(x => x.fg_urgente).ThenBy(x => x.dtcontato).ThenBy(x => x.Hora).Select(s => new DadosAgen
                                       {
                                           agendamentoid = s.agendamentoid,
                                           fg_urgente = s.fg_urgente,
                                           contato = s.contatoid,
                                           hora = s.Hora == null ? "" : s.Hora,
                                           consultornome = s.nomeconsultor == null ? "" : s.nomeconsultor + " " + s.sobrenomeconsultor,
                                           nome = s.nome == null || s.nome == "" ? "" : s.nome.ToUpper(),
                                           fone1 = s.fone1 == null ? "" : s.fone1,
                                           fone2 = s.fone2 == null ? "" : s.fone2,
                                           dtcontato = s.dtcontato.ToString(),
                                           tipocliente = (s.tipo == "C" ? "Venda" : s.tipo == "S" ? "Solicitação" : s.tipo == "I" ? "Simulação" : "Ligação"),
                                           Classificacao = s.Classificacao
                                       }).ToList();

                return busca;
            }
            else
            {
                var busca = banco.vw_tarefas.Where(a => (a.fg_finalizado == finalizado) &&
                               (a.tipo == tipo || tipo == "") &&
                               (a.nome.Contains(cliente) || cliente == "") &&
                               (a.dtcontato >= dtde && a.dtcontato <= dtate || data == "" || dataate == "")).OrderByDescending(x => x.fg_urgente).ThenBy(x => x.dtcontato).ThenBy(x => x.Hora).Select(s => new DadosAgen
                               {
                                   agendamentoid = s.agendamentoid,
                                   fg_urgente = s.fg_urgente,
                                   contato = s.contatoid,
                                   hora = s.Hora == null ? "" : s.Hora,
                                   consultornome = s.nomeconsultor == null ? "" : s.nomeconsultor + " " + s.sobrenomeconsultor,
                                   nome = s.nome == null || s.nome == "" ? "" : s.nome.ToUpper(),
                                   fone1 = s.fone1 == null ? "" : s.fone1,
                                   fone2 = s.fone2 == null ? "" : s.fone2,
                                   dtcontato = s.dtcontato.ToString(),
                                   tipocliente = (s.tipo == "C" ? "Venda" : s.tipo == "S" ? "Solicitação" : s.tipo == "I" ? "Simulação" : "Ligação"),
                                   Classificacao = s.Classificacao
                               }).ToList();
                return busca;
            }
        }

        public DadosAgen DetalheAgendamentoBD(string id)
        {
            int idagend = int.Parse(id);
            var dtatent = "";
            var dtexpd = "";

            var list = banco.Agendamentoes.Where(x => x.AgendamentoId == idagend).FirstOrDefault();
            var contatos = banco.Contatos.Where(y => y.AgendamentoID == idagend).Select(w => new ContatosVM
            {
                agendamentoid = w.AgendamentoID,
                contadoid = w.ContatoID,
                hora = w.Hora,
                dtcontato = w.DtContato.ToString(),
                observacao = w.Observacao,
            });

            if (list.DtAtendimento != null)
            {
                char[] splitdt = { '/' };
                dtatent = list.DtAtendimento.ToString();
                dtatent = dtatent.Replace("00:00:00", " ");
                string[] splitdtatend = dtatent.Split(splitdt);
                dtatent = splitdtatend[2] + "-" + splitdtatend[1] + "-" + splitdtatend[0];
                dtatent = dtatent.Replace(" ", "");
            }

            if (list.DataExpedicao != null)
            {
                char[] splitdt = { '/' };
                dtexpd = list.DataExpedicao.ToString();
                dtexpd = dtexpd.Replace("00:00:00", " ");
                string[] splitdexped = dtexpd.Split(splitdt);
                dtexpd = splitdexped[2] + "-" + splitdexped[1] + "-" + splitdexped[0];
                dtexpd = dtexpd.Replace(" ", "");
            }

            DadosAgen a = new DadosAgen();

            if (list.PessoaFisica == true)
            {
                a.Operacaof = list.Operacao;
                a.complemento = list.Complemento;
                a.nomeconjugue = list.NomeConjuge;
                a.cpfconjugue = list.CpfConjuge;
                a.dtnascimentoconjugue = list.DtNascimentoConjugue;
                a.dtnascimento = list.DtNascimento;
                a.agendamentoid = list.AgendamentoId;
                a.tipocliente = list.Tipo;
                a.nome = list.Nome == null || list.Nome == "" ? "" : list.Nome.ToString().ToUpper();
                a.fone1 = list.Fone1;
                a.fone2 = list.Fone2;
                a.email = list.Email;
                a.dtatendimento = list.DtAtendimento;
                a.consultor = list.ConsultorId;
                a.autocheck = (list.Auto == true ? "checked" : " ");
                a.imocheck = (list.Imobiliario == true ? "checked" : " ");
                a.valorimo = list.ValorImo;
                a.valorauto = list.ValorAuto;
                a.tiporadiopessoa = list.PessoaFisica;
                a.Classificacao = list.Classificacao;
                a.tiporadiopessoastring = list.PessoaFisica == true ? "Sim" : "Não";
                a.cpff = list.Cpf_Cnpj;
                a.rgf = list.RgNro;
                a.orgaoemissorf = list.RgEmissor;
                a.dtexpedicao = list.DataExpedicao;
                a.cepf = list.CEP;
                a.enderecof = list.Logradouro;
                a.numerof = list.Numero;
                a.bairrof = list.Bairro;
                a.cidadef = list.Cidade;
                a.uff = list.Uf;
                a.estadocivilf = list.EstadoCivilFk;
                a.profissaof = list.Profissao;
                a.cnascimentof = list.CidNascimento;
                a.nacionalidadef = list.Nacionalidade;
                a.fonefixof = list.FoneFixo;
                a.celularf = list.Celular;
                a.bancof = list.Banco;
                a.agenciaf = list.Agencia;
                a.contaf = list.Conta;
                a.formapagamentof = list.FormaDePagamentoFk;
                a.dtatendimentostring = dtatent;
                a.dtexpedicaostring = dtexpd;
                a.contatos = contatos.ToList();
                a.AgendamentoSituacaoFK = list.AgendamentoSituacaoFK;
                a.fg_finalizado = list.fg_Finalizado;
            }
            else if (list.PessoaFisica == null)
            {
                a.Operacaoj = list.Operacao;
                a.complemento = list.Complemento;
                a.dtnascimento = list.DtNascimento;
                a.agendamentoid = list.AgendamentoId;
                a.tipocliente = list.Tipo;
                a.nome = list.Nome == null || list.Nome == "" ? "" : list.Nome.ToString().ToUpper();
                a.fone1 = list.Fone1;
                a.fone2 = list.Fone2;
                a.email = list.Email;
                a.dtatendimento = list.DtAtendimento;
                a.consultor = list.ConsultorId;
                a.autocheck = (list.Auto == true ? "checked" : " ");
                a.imocheck = (list.Imobiliario == true ? "checked" : " ");
                a.valorimo = list.ValorImo;
                a.valorauto = list.ValorAuto;
                a.tiporadiopessoa = list.PessoaFisica;
                a.tiporadiopessoastring = "Sim";
                a.cnpjj = list.Cpf_Cnpj;
                a.razaosocialj = list.RazaoSocial;
                a.nomefantasiaj = list.NomeFantasia;
                a.fundacaoj = list.Fundacao;
                a.capitalsocialj = list.CapitalSocial;
                a.tipocapitalj = list.TipoCapital;
                a.atividadej = list.Atividade;
                a.nomesocioj = list.NomeSocio;
                a.cargosocioj = list.CargoSocio;
                a.cpfsocioj = list.CPFSocio;
                a.particsocioj = list.ParticipacaoSocio;
                a.cepj = list.CEP;
                a.ruaj = list.Logradouro;
                a.numeroj = list.Numero;
                a.bairroj = list.Bairro;
                a.cidadej = list.Cidade;
                a.ufj = list.Uf;
                a.fonefixoj = list.FoneFixo;
                a.celularj = list.Celular;
                a.bancoj = list.Banco;
                a.agenciaj = list.Agencia;
                a.contaj = list.Conta;
                a.formapagamentoj = list.FormaDePagamentoFk;
                a.dtatendimentostring = dtatent;
                a.contatos = contatos.ToList();
                a.AgendamentoSituacaoFK = list.AgendamentoSituacaoFK;
                a.fg_finalizado = list.fg_Finalizado;
            }
            else
            {
                a.Operacaoj = list.Operacao;
                a.complemento = list.Complemento;
                a.dtnascimento = list.DtNascimento;
                a.agendamentoid = list.AgendamentoId;
                a.tipocliente = list.Tipo;
                a.nome = list.Nome == null || list.Nome == "" ? "" : list.Nome.ToString().ToUpper();
                a.fone1 = list.Fone1;
                a.fone2 = list.Fone2;
                a.email = list.Email;
                a.dtatendimento = list.DtAtendimento;
                a.consultor = list.ConsultorId;
                a.autocheck = (list.Auto == true ? "checked" : " ");
                a.imocheck = (list.Imobiliario == true ? "checked" : " ");
                a.valorimo = list.ValorImo;
                a.valorauto = list.ValorAuto;
                a.tiporadiopessoa = list.PessoaFisica;
                a.tiporadiopessoastring = list.PessoaFisica == true ? "Sim" : "Não";
                a.cnpjj = list.Cpf_Cnpj;
                a.razaosocialj = list.RazaoSocial;
                a.nomefantasiaj = list.NomeFantasia;
                a.fundacaoj = list.Fundacao;
                a.capitalsocialj = list.CapitalSocial;
                a.tipocapitalj = list.TipoCapital;
                a.atividadej = list.Atividade;
                a.nomesocioj = list.NomeSocio;
                a.cargosocioj = list.CargoSocio;
                a.cpfsocioj = list.CPFSocio;
                a.particsocioj = list.ParticipacaoSocio;
                a.cepj = list.CEP;
                a.ruaj = list.Logradouro;
                a.numeroj = list.Numero;
                a.bairroj = list.Bairro;
                a.cidadej = list.Cidade;
                a.ufj = list.Uf;
                a.fonefixoj = list.FoneFixo;
                a.celularj = list.Celular;
                a.bancoj = list.Banco;
                a.agenciaj = list.Agencia;
                a.contaj = list.Conta;
                a.formapagamentoj = list.FormaDePagamentoFk;
                a.dtatendimentostring = dtatent;
                a.contatos = contatos.ToList();
                a.AgendamentoSituacaoFK = list.AgendamentoSituacaoFK;
                a.fg_finalizado = list.fg_Finalizado;
            }

            a.fg_urgente = list.fg_Urgente;
            a.numcontrato = list.NumContrato.ToString();
            a.protocolo = list.Protocolo;
            return a;
        }

        public string SalvarAlteracaoBD(AgendamentosVM dados)
        {
            int id = 0;
            Agendamento retorno = null;

            if (dados.agendamento[0].dtatendimento == null)
            {
                dados.agendamento[0].dtatendimento = DateTime.Now;
            }

            //Agendamento Novo
            if (dados.agendamento[0].agendamentoid == 0)
            {
                retorno = new Agendamento();
                retorno.fg_Ativo = "1";
                banco.Agendamentoes.Add(retorno);
                id = retorno.AgendamentoId;
            }
            //Agendamento Pre Existente (Edicao do Agendamento
            else
            {
                id = dados.agendamento[0].agendamentoid;

                retorno = banco.Agendamentoes.Where(x => x.AgendamentoId == id).FirstOrDefault();

                if (retorno.Tipo != dados.agendamento[0].tipocliente)
                {
                    var agendamentoHistorico = new AgendamentoHistorico
                    {
                        AgendamentoId = id,
                        ConsultorId = retorno.ConsultorId,
                        TipoInicial = retorno.Tipo,
                        TipoFinal = dados.agendamento[0].tipocliente,
                        CriadoEm = DateTime.Now
                    };
                    retorno.AgendamentoHistoricoes.Add(agendamentoHistorico);
                }
            }

            try
            {
                if (dados.agendamento != null)
                {
                    if (dados.agendamento[0].tiporadiopessoastring == "pfisica")
                    {
                        retorno.Classificacao = dados.agendamento[0].Classificacao;
                        retorno.Operacao = dados.agendamento[0].Operacaof;
                        retorno.DtNascimento = dados.agendamento[0].dtnascimento;
                        retorno.Nome = dados.agendamento[0].nome;
                        retorno.Classificacao = dados.agendamento[0].Classificacao;
                        retorno.Fone1 = dados.agendamento[0].fone1;
                        retorno.Fone2 = dados.agendamento[0].fone2;
                        retorno.Email = dados.agendamento[0].email;
                        retorno.DtAtendimento = dados.agendamento[0].dtatendimento;
                        retorno.ConsultorId = dados.agendamento[0].consultor;
                        retorno.Auto = bool.Parse(dados.agendamento[0].autocheck);

                        if (dados.agendamento[0].dtexpedicao != null)
                        {
                            retorno.DataExpedicao = dados.agendamento[0].dtexpedicao;
                        }
                        retorno.Complemento = dados.agendamento[0].complemento;
                        retorno.Imobiliario = bool.Parse(dados.agendamento[0].imocheck);
                        retorno.Cpf_Cnpj = dados.agendamento[0].cpff;
                        retorno.RgNro = dados.agendamento[0].rgf;
                        retorno.RgEmissor = dados.agendamento[0].orgaoemissorf;
                        retorno.Logradouro = dados.agendamento[0].enderecof;
                        retorno.Numero = dados.agendamento[0].numerof;
                        retorno.Bairro = dados.agendamento[0].bairrof;
                        retorno.Cidade = dados.agendamento[0].cidadef;
                        retorno.Uf = dados.agendamento[0].uff;
                        if (dados.agendamento[0].estadocivilf != null && dados.agendamento[0].estadocivilf != 0)
                        {
                            retorno.EstadoCivilFk = dados.agendamento[0].estadocivilf;
                        }
                        retorno.NomeConjuge = dados.agendamento[0].nomeconjugue;
                        retorno.CpfConjuge = dados.agendamento[0].cpfconjugue;
                        retorno.DtNascimentoConjugue = dados.agendamento[0].dtnascimentoconjugue;
                        retorno.Profissao = dados.agendamento[0].profissaof;
                        retorno.CidNascimento = dados.agendamento[0].cnascimentof;
                        retorno.Nacionalidade = dados.agendamento[0].nacionalidadef;
                        retorno.FoneFixo = dados.agendamento[0].fonefixof;
                        retorno.Celular = dados.agendamento[0].celularf;
                        retorno.Banco = dados.agendamento[0].bancof;
                        retorno.Agencia = dados.agendamento[0].agenciaf;
                        retorno.Conta = dados.agendamento[0].contaf;
                        retorno.FormaDePagamentoFk = dados.agendamento[0].formapagamentof;
                        retorno.PessoaFisica = true;
                        retorno.CEP = dados.agendamento[0].cepf;
                        retorno.AgendamentoSituacaoFK = dados.agendamento[0].AgendamentoSituacaoFK == 0 ? null : dados.agendamento[0].AgendamentoSituacaoFK;
                        if (dados.agendamento[0].valorauto != null)
                        {
                            retorno.ValorAuto = dados.agendamento[0].valorauto;
                        }
                        if (dados.agendamento[0].valorimo != null)
                        {
                            retorno.ValorImo = dados.agendamento[0].valorimo;
                        }

                        retorno.Tipo = dados.agendamento[0].tipocliente;
                        if (dados.agendamento[0].numcontrato != null && dados.agendamento[0].numcontrato != "")
                        {
                            retorno.NumContrato = Int32.Parse(dados.agendamento[0].numcontrato);
                        }
                        retorno.Protocolo = dados.agendamento[0].protocolo;
                    }
                    else if (dados.agendamento[0].tiporadiopessoastring == "pjuridica")
                    {
                        retorno.Classificacao = dados.agendamento[0].Classificacao;
                        retorno.Operacao = dados.agendamento[0].Operacaoj;
                        retorno.DtNascimento = dados.agendamento[0].dtnascimento;
                        retorno.Nome = dados.agendamento[0].nome;
                        retorno.Fone1 = dados.agendamento[0].fone1;
                        retorno.Fone2 = dados.agendamento[0].fone2;
                        retorno.Email = dados.agendamento[0].email;
                        retorno.DtAtendimento = dados.agendamento[0].dtatendimento;
                        retorno.ConsultorId = dados.agendamento[0].consultor;
                        retorno.Auto = bool.Parse(dados.agendamento[0].autocheck);
                        retorno.Imobiliario = bool.Parse(dados.agendamento[0].imocheck);

                        if (dados.agendamento[0].valorauto != null)
                        {
                            retorno.ValorAuto = dados.agendamento[0].valorauto;
                        }
                        if (dados.agendamento[0].valorimo != null)
                        {
                            retorno.ValorImo = dados.agendamento[0].valorimo;
                        }
                        retorno.Complemento = dados.agendamento[0].complemento;
                        retorno.Cpf_Cnpj = dados.agendamento[0].cnpjj;
                        retorno.RazaoSocial = dados.agendamento[0].razaosocialj;
                        retorno.NomeFantasia = dados.agendamento[0].nomefantasiaj;
                        retorno.Fundacao = dados.agendamento[0].fundacaoj;
                        retorno.CapitalSocial = dados.agendamento[0].capitalsocialj;
                        retorno.TipoCapital = dados.agendamento[0].tipocapitalj;
                        retorno.Atividade = dados.agendamento[0].atividadej;
                        retorno.NomeSocio = dados.agendamento[0].nomesocioj;
                        retorno.CargoSocio = dados.agendamento[0].cargosocioj;
                        retorno.CPFSocio = dados.agendamento[0].cpfsocioj;
                        retorno.ParticipacaoSocio = dados.agendamento[0].particsocioj;
                        retorno.CEP = dados.agendamento[0].cepj;
                        retorno.AgendamentoSituacaoFK = dados.agendamento[0].AgendamentoSituacaoFK == 0 ? null : dados.agendamento[0].AgendamentoSituacaoFK;
                        retorno.Logradouro = dados.agendamento[0].ruaj;
                        retorno.Bairro = dados.agendamento[0].bairroj;
                        retorno.Numero = dados.agendamento[0].numeroj;
                        retorno.Cidade = dados.agendamento[0].cidadej;
                        retorno.Uf = dados.agendamento[0].ufj;
                        retorno.FoneFixo = dados.agendamento[0].fonefixoj;
                        retorno.Celular = dados.agendamento[0].celularj;
                        retorno.Banco = dados.agendamento[0].bancoj;
                        retorno.Agencia = dados.agendamento[0].agenciaj;
                        retorno.Conta = dados.agendamento[0].contaj;
                        retorno.FormaDePagamentoFk = dados.agendamento[0].formapagamentoj;
                        if (dados.agendamento[0].numcontrato != null && dados.agendamento[0].numcontrato != "")
                        {
                            retorno.NumContrato = Int32.Parse(dados.agendamento[0].numcontrato);
                        }
                        retorno.Protocolo = dados.agendamento[0].protocolo;
                        retorno.Tipo = dados.agendamento[0].tipocliente;
                    }
                    retorno.fg_Urgente = dados.agendamento[0].fg_urgente;
                }

                var retornocontados = banco.Contatos.Where(x => x.AgendamentoID == id).FirstOrDefault();
                Contato c = null;

                if (dados.contatos != null)
                {
                    int quantcontatos = dados.contatos.Count();

                    if (quantcontatos != 0)
                    {
                        int aux = 0;
                        for (int i = 0; i < quantcontatos; i++)
                        {
                            if (aux < dados.contatos[i].contfirst)
                            {
                                var idcont = Int32.Parse(dados.contatos[i].contadoid.ToString());
                                var contold = idcont == 0 ? new Contato() : banco.Contatos.Where(l => l.ContatoID == idcont).FirstOrDefault();

                                if (idcont == 0)
                                {
                                    banco.Contatos.Add(contold);
                                }

                                if (dados.contatos[i].dtcontato != "")
                                {
                                    contold.Hora = dados.contatos[i].hora;
                                    contold.DtContato = DateTime.Parse(dados.contatos[i].dtcontato);
                                    contold.Observacao = dados.contatos[i].observacao;
                                    retorno.UltimoContato = idcont;
                                    aux++;
                                }
                            }
                            else if (aux > dados.contatos[i].contfirst || aux < dados.contatos[i].contsec)
                            {
                                if (dados.contatos[i].dtcontato != "")
                                {
                                    c = new Contato();
                                    c.Hora = dados.contatos[i].hora;
                                    c.AgendamentoID = dados.agendamento[0].agendamentoid;
                                    c.Observacao = dados.contatos[i].observacao;
                                    c.DtContato = DateTime.Parse(dados.contatos[i].dtcontato);
                                    aux++;
                                    var ab = banco.Contatos.Add(c);
                                    banco.SaveChanges();
                                    retorno.UltimoContato = ab.ContatoID;
                                }
                            }
                        }
                    }
                }

                banco.SaveChanges();
                return "1";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return "0";
            }
        }

        public IQueryable<AgendamentoHistorico> CriaQueryHistoricoAgendamento(IQueryable<AgendamentoHistorico> @this, BuscarAgendamentoHistoricoVM requestModel)
        {
            if (!string.IsNullOrEmpty(requestModel.Tipo) && !requestModel.Tipo.Equals("0"))
            {
                @this = @this
                .Where(x => x.TipoInicial == requestModel.Tipo);
            }

            if (!requestModel.DataInicio.Equals(DateTime.MinValue))
            {
                @this = @this.Where(x => x.CriadoEm >= requestModel.DataInicio);
            }

            if (!requestModel.DataFim.Equals(DateTime.MinValue))
            {
                var dataFim = requestModel.DataFim
               .AddHours(23)
               .AddMinutes(59)
               .AddSeconds(59);

                @this = @this.Where(x => x.CriadoEm <= dataFim);
            }

            if (!string.IsNullOrEmpty(requestModel.Cliente))
            {
                @this = @this.Where(x => x.Agendamento.Nome.Contains(requestModel.Cliente));
            }

            return @this;
        }

        public IEnumerable<AgendamentoHistorico> ListaAgendamentosHistoricoPorUserId(int userId)
        {
            var agendamentos = ListaAgendamentoHistoricoPorUserId(userId);
            var tipoAgendamento = ListarTiposAgendamento();

            agendamentos.ForEachAsync(
                x =>
                {
                    x.TipoFinal = tipoAgendamento.FirstOrDefault(t => t.Id == x.TipoFinal)?.Descricao ?? string.Empty;
                    x.TipoInicial = tipoAgendamento.FirstOrDefault(t => t.Id == x.TipoInicial)?.Descricao ?? string.Empty;
                })
                .GetAwaiter()
                .GetResult();

            return agendamentos.ToList();
        }

        public IEnumerable<AgendamentoHistorico> ListaHistoricoAgendamento()
        {
            var agendamentos = banco.AgendamentoHistoricoes;
            var tipoAgendamento = ListarTiposAgendamento();

            agendamentos.ToList().ForEach(
                 x =>
                 {
                     x.TipoFinal = tipoAgendamento.FirstOrDefault(t => t.Id == x.TipoFinal)?.Descricao ?? string.Empty;
                     x.TipoInicial = tipoAgendamento.FirstOrDefault(t => t.Id == x.TipoInicial)?.Descricao ?? string.Empty;
                 });
            return agendamentos;
        }

        public IEnumerable<AgendamentoHistorico> ListaHistoricoAgendamento(BuscarAgendamentoHistoricoVM requestModel)
        {
            var agendamentos = banco.AgendamentoHistoricoes.AsQueryable();
            var tipoAgendamento = ListarTiposAgendamento();

            agendamentos = CriaQueryHistoricoAgendamento(agendamentos, requestModel);

            if (requestModel.ConsultorId > 0)
            {
                agendamentos = agendamentos.Where(x => x.ConsultorId == requestModel.ConsultorId);
            }

            agendamentos.ToList().ForEach(
                 x =>
                 {
                     x.TipoFinal = tipoAgendamento.FirstOrDefault(t => t.Id == x.TipoFinal)?.Descricao ?? string.Empty;
                     x.TipoInicial = tipoAgendamento.FirstOrDefault(t => t.Id == x.TipoInicial)?.Descricao ?? string.Empty;
                 });
            return agendamentos.ToList();
        }

        public IEnumerable<AgendamentoHistorico> ListaHistoricoAgendamento(BuscarAgendamentoHistoricoVM requestModel, int userId)
        {
            var agendamentos = ListaAgendamentoHistoricoPorUserId(userId);
            var tipoAgendamento = ListarTiposAgendamento();

            agendamentos = CriaQueryHistoricoAgendamento(agendamentos, requestModel);

            agendamentos.ToList().ForEach(
                 x =>
                 {
                     x.TipoFinal = tipoAgendamento.FirstOrDefault(t => t.Id == x.TipoFinal)?.Descricao ?? string.Empty;
                     x.TipoInicial = tipoAgendamento.FirstOrDefault(t => t.Id == x.TipoInicial)?.Descricao ?? string.Empty;
                 });
            return agendamentos.ToList();
        }

        private IQueryable<AgendamentoHistorico> ListaAgendamentoHistoricoPorUserId(int userId)
            => banco.AgendamentoHistoricoes.Where(x => x.ConsultorId == userId);

        public IEnumerable<TipoAgendamento> ListarTiposAgendamento()
        => banco.TipoAgendamentoes.ToList();
    }
}