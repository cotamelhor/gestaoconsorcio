﻿using GestaoConsorcio.Models;
using GestaoConsorcio.Models.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using GestaoConsorcio.Models.Banco;
using System.Data.Entity;
using System.Reflection;
using System.Data;
using System.Text.RegularExpressions;

namespace GestaoConsorcio.Models.Repositorio
{
    public class CampanhasBD
    {
        private db_gestaocotamelhor_desenvEntities banco = new db_gestaocotamelhor_desenvEntities();

        public int ContarCampanhas(int pag = 0, int usuid = 0, int campanhafk = 0, DateTime? inicio = null, DateTime? fim = null)
        {
            List<ContatoCampanha> temp = banco.ContatoCampanhas.Where(x => x.Ativo == true
                                                    && (usuid == 0 || x.UsuarioFK == usuid)
                                                    && (campanhafk == 0 || x.CampanhaFK == campanhafk)
                                                    && (inicio == null || (x.CriadoEm >= inicio && x.CriadoEm <= fim))).ToList();
            return temp.Count;
        }

        public List<ContatoCampanha> BuscarContatosCampanhas(int pag = 0, int usuid = 0, int campanhafk = 0, DateTime? inicio = null, DateTime? fim = null)
        {
            List<ContatoCampanha> temp = banco.ContatoCampanhas.Where(x => x.Ativo == true
                                                    && (usuid == 0 || x.UsuarioFK == usuid)
                                                    && (campanhafk == 0 || x.CampanhaFK == campanhafk)
                                                    && (inicio == null || (x.CriadoEm >= inicio && x.CriadoEm <= fim))).ToList();
            return temp.Skip((pag - 1) * 15).Take(15).ToList();
        }


        public ContatoCampanha BuscarContatoPorID(int contatoid)
        {
            return banco.ContatoCampanhas.Where(x => x.ContatoCampanhaID == contatoid).FirstOrDefault();
        }

        public void Salvar(ContatoCampanha contato)
        {

            var obj = banco.ContatoCampanhas.Where(x => x.ContatoCampanhaID == contato.ContatoCampanhaID).FirstOrDefault();

            obj.Telefone = contato.Telefone;
            obj.Email = contato.Email;
            obj.Renda = contato.Renda;
            obj.ValoresCarta = contato.ValoresCarta;
            obj.ValoresParcela = contato.ValoresParcela;
            obj.UsuarioFK = contato.UsuarioFK;

            banco.SaveChanges();
        }


        public List<ObservacaoContato> ListarObservacoes(int contatoid)
        {
            return banco.ObservacaoContatoes.Where(x => x.ContatoCampanhaFK == contatoid).ToList();
        }

        public void SalvarObs(int usuid, int contatoid, string txt, DateTime data)
        {
            banco.ObservacaoContatoes.Add(new ObservacaoContato() {
                UsuarioFK = usuid,
                ContatoCampanhaFK = contatoid,
                CriadoEm = data,
                Texto = txt
            });
            banco.SaveChanges();
        }

        public void AssociarTagContato(int contatoid, int tagid)
        {
            if (banco.CCampTags.Where(x => x.ContatoCampanhaFK == contatoid && x.TagContatoFK == tagid).Count() <= 0)
            {
                banco.CCampTags.Add(new CCampTag() { ContatoCampanhaFK = contatoid, TagContatoFK = tagid });
                banco.SaveChanges();
            }
            
        }

        public void ApagarTagContato(int contatoid, int tagid)
        {
            var obj = banco.CCampTags.Where(x => x.ContatoCampanhaFK == contatoid && x.TagContatoFK == tagid).FirstOrDefault();
            if (obj != null)
            {
                banco.CCampTags.Remove(obj);
                banco.SaveChanges();
            }

        }

        public bool VerificarTagContato(int contatoid, int tagid)
        {
            return banco.CCampTags.Where(x => x.ContatoCampanhaFK == contatoid && x.TagContatoFK == tagid).Count() > 0;
            
        }

        private void SalvarObservacaoContato(DataTable tab, DataRow row, String fieldname, int contatocampanhaid, int usuarioid)
        {
            if (tab.Columns.Contains(fieldname))
            {
                banco.ObservacaoContatoes.Add(new ObservacaoContato()
                {
                    Texto = row[fieldname].ToString(),
                    ContatoCampanhaFK = contatocampanhaid,
                    CriadoEm = DateTime.Now,
                    UsuarioFK = usuarioid
                });

            }
        }

        public void SalvarImportacao(DataTable tab, int usuarioid, int campanhaid)
        {
            string[] masculino = { "1", "M" };

            foreach(DataRow row in tab.Rows)
            {
                try
                {
                    ContatoCampanha cc = new ContatoCampanha()
                    {
                        CriadoEm = DateTime.Now,
                        UsuarioFK = usuarioid,
                        CampanhaFK = campanhaid,
                        Ativo = true
                    };

                    if (tab.Columns.Contains("Nome")) cc.Nome = row["Nome"].ToString();
                    if (tab.Columns.Contains("Telefone")) cc.Telefone = Regex.Replace(row["Telefone"].ToString(),"[^0-9]", "");
                    if (tab.Columns.Contains("Email")) cc.Email = row["Email"].ToString();
                    if (tab.Columns.Contains("Estado")) cc.Estado = row["Estado"].ToString();
                    if (tab.Columns.Contains("FeitoEm")) cc.FeitoEm = row["FeitoEm"].ToString();
                    if (tab.Columns.Contains("Renda")) cc.Renda = row["Renda"].ToString();
                    if (tab.Columns.Contains("ValoresCarta")) cc.ValoresCarta = row["ValoresCarta"].ToString();
                    if (tab.Columns.Contains("ValoresParcela")) cc.ValoresParcela = row["ValoresParcela"].ToString();
                    if (tab.Columns.Contains("Sexo")) cc.Sexo = masculino.Contains(row["Sexo"].ToString().ToUpper().Trim());
                    if (tab.Columns.Contains("MelhorHorario")) cc.MelhorHorario = row["MelhorHorario"].ToString();
                    if (tab.Columns.Contains("OrigemTxt")) cc.OrigemTxt = row["OrigemTxt"].ToString();
                    if (tab.Columns.Contains("Texto1")) cc.Texto1 = row["Texto1"].ToString();
                    if (tab.Columns.Contains("Texto2")) cc.Texto2 = row["Texto2"].ToString();

                    var edcc = banco.ContatoCampanhas.Where(x => x.Email == cc.Email).FirstOrDefault();

                    if (edcc == null)
                    {
                        edcc.Nome = cc.Nome;
                        banco.SaveChanges();
                    }
                    else
                    {
                        banco.ContatoCampanhas.Add(cc);
                        banco.SaveChanges();

                        SalvarObservacaoContato(tab, row, "Obs1", cc.ContatoCampanhaID, usuarioid);
                        SalvarObservacaoContato(tab, row, "Obs2", cc.ContatoCampanhaID, usuarioid);
                        SalvarObservacaoContato(tab, row, "Obs3", cc.ContatoCampanhaID, usuarioid);

                    }

                }
                catch (Exception e){

                    Console.WriteLine(e);

                }
            }
            
        }

    }
}