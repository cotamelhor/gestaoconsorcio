﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestaoConsorcio.Models.Repositorio;
using GestaoConsorcio.Models.View;
using GestaoConsorcio.Models.Banco;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using GestaoConsorcio.Models;

namespace GestaoConsorcio.Models.Repositorio
{
    public class ContratoBD
    {

        private db_gestaocotamelhor_desenvEntities banco = new db_gestaocotamelhor_desenvEntities();

        public string Simulacao(SimulacaoVM model)
        {
            try
            {
                Agendamento ag = new Agendamento();

                ag.Email = model.Email;
                ag.Protocolo = model.ProtocoloAtends;
                ag.fg_Ativo = "1";
                ag.Tipo = "I";
                ag.fg_Urgente = false;
                ag.ConsultorId = model.Consultor;
                ag.DtAtendimento = DateTime.Now;

                var id = banco.Agendamentoes.Add(ag);

                Contato cont = new Contato();

                cont.AgendamentoID = id.AgendamentoId;
                cont.DtContato = DateTime.Now;
                cont.Hora = DateTime.Now.Hour + ":" + (Int32.Parse(DateTime.Now.Minute.ToString()) < 10 ? "0" + DateTime.Now.Minute.ToString() : DateTime.Now.Minute.ToString());
                cont.Observacao = model.Observacao;
                var idcont = banco.Contatos.Add(cont);
                banco.SaveChanges();

                ag.UltimoContato = idcont.ContatoID;
                banco.SaveChanges();

                return "1";
            }
            catch
            {
                return "-1";
            }
            
        }

        public string Solicitacao(DadosAgen model)
        {
            try
            {

                var existcontrato = banco.Agendamentoes.Where(x => x.Protocolo == model.ProtocolocoAtendimento).FirstOrDefault();
                Banco.Agendamento ct = new Banco.Agendamento();

                if (existcontrato == null)
                {
                    ct.Protocolo = model.ProtocolocoAtendimento;
                    ct.DtAtendimento = DateTime.Now;
                    ct.fg_Ativo = "1";
                    ct.Tipo = "S";
                    ct.fg_Urgente = false;
                    ct.ConsultorId = model.consultor;
                    var id = banco.Agendamentoes.Add(ct);

                    Contato cont = new Contato();

                    cont.AgendamentoID = id.AgendamentoId;
                    cont.DtContato = DateTime.Now;
                    cont.Hora = DateTime.Now.Hour + ":" + (Int32.Parse(DateTime.Now.Minute.ToString()) < 10 ? "0" + DateTime.Now.Minute.ToString() : DateTime.Now.Minute.ToString());
                    var idcont = banco.Contatos.Add(cont);
                    banco.SaveChanges();

                    ct.UltimoContato = idcont.ContatoID;
                    banco.SaveChanges();

                    return "1";
                }
                else
                {

                    return "0";
                }
            }
            catch
            {
                return "-1";
            }
        }

        public string ExisteContrato(SalvarContratoVM cont)
        {
            Banco.Contrato tbCont = null;
            int a = Int32.Parse(cont.NumeroContrato);
            string msg = null;


                tbCont = new Banco.Contrato();
                var exit = banco.Contratoes.Where(x => x.NumContrato == a).FirstOrDefault();
                if (exit == null)
                {
                    try
                    {
                        tbCont.Consultor = cont.ConsultorID;
                        tbCont.NumContrato = Int32.Parse(cont.NumeroContrato);
                        tbCont.DtInclusao = DateTime.Now;
                        tbCont.ProtocoloAtend = cont.ProtocoloAtends;
                        banco.Contratoes.Add(tbCont);

                        banco.SaveChanges();
                        msg = "1";            
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        msg = "0";
                    }
                }
                else
                {
                    msg = "-1";
                }
          
            return msg;
        }

        public List<ContratoVM> BuscarContrato(string contrato, string nome, string checkbox, DateTime? inicio = null, DateTime? fim = null)
        {
            List<ContratoVM> retorno = null;
            int cont = 0;
            bool check = true;

            if(contrato != "" && contrato != null)
            {
                cont = Int32.Parse(contrato);
            }
            if (checkbox == "Sim,Nao" || checkbox == null || checkbox == "")
            {
                checkbox = "dois";
            }

            if (checkbox == "dois")
            {

                retorno = banco.Contratoes.Where(x => (x.Nome.Contains(nome) || nome == "" || nome == null) &&
                                                      (!inicio.HasValue || !x.DtAdesao.HasValue || x.DtAdesao >= inicio) &&
                                                      (!fim.HasValue || !x.DtAdesao.HasValue || x.DtAdesao <= fim) &&
                                                     (x.NumContrato == cont || cont == 0)).ToList().Select(y => new ContratoVM
                                                     {
                                                         id = y.Id,
                                                         nome = y.Nome == null ? "" : y.Nome,
                                                         NumeroContrato = y.NumContrato.ToString(),
                                                         CheckBoxAlocado = y.Alocado == true ? "checked" : "",
                                                         DtAdesao = y.DtAdesao.HasValue ? y.DtAdesao.Value.ToString("dd/MM/yyyy") : null,
                                                         DtAlocacao = y.DtAlocacao.HasValue ? y.DtAlocacao.Value.ToString("dd/MM/yyyy") : null
                                                     }).ToList();

                
            }
            else
            {
                check = checkbox == "Sim" ? true : false;
                retorno = banco.Contratoes.Where(x => x.Alocado == check &&
                                                     (x.Nome.Contains(nome) || nome == "" || nome == null) &&
                                                     (x.NumContrato == cont || cont == 0)).Select(y => new ContratoVM
                                                     {
                                                         id = y.Id,
                                                         nome = y.Nome == null ? "" : y.Nome,
                                                         NumeroContrato = y.NumContrato.ToString(),
                                                         CheckBoxAlocado = y.Alocado == true ? "checked" : ""

                                                     }).ToList();
            }


            return retorno;
            
        }

        public List<ContratoVM> DetalheContrato(int id)
        {
            var cont = banco.Contratoes.Where(x => x.Id == id).Select(y => new ContratoVM()
            {
                BemObjeto = y.BemObjeto,
                Cont = y.Cont,
                NumeroContrato = y.NumContrato.ToString(),
                cota = y.Cota,
                cpf = y.Cnpj_Cpf,
                DtAdesao = y.DtAdesao.ToString(),
                DtAlocacao = y.DtAlocacao.ToString(),
                DtAssembleia = y.DtAssembleia.ToString(),
                DtCadastro = y.DtCadastro.ToString(),
                DtNascimento = y.DtNascimento.ToString(),
                FndReserva = y.FndReserva,
                grupo = y.Grupo,
                nome = y.Nome,
                PlanoBasico = y.PlanoBasico,
                PlanoVenda = y.PlanoVenda,
                Seguros = y.Seguros.ToString(),
                sitcobranca = y.SitCobranca,
                situacao = y.Situacao,
                TaxaAdesao = y.TaxaAdesao,
                TaxaAdm = y.TaxaAdm,
                tipocota = y.TipoCota,
                TipoVenda = y.TipoVenda,
                DtAssembleiaAtual = y.DtAssembleiaAtual.ToString(),
                AssembleiaAtual = y.AssembleiaAtual.ToString(),
                Vencto = y.Vencto.ToString(),
                ValoresPgos = y.ValoresPgos,
                SaldoDevedor = y.SaldoDevedor,
                Atraso = y.Atraso,
                ValorParcela = y.ValorParcela,
                NumeroParcel = y.NumeroParcel.ToString(),
                QtdPclsPagas = y.QtdPclsPagas.ToString()
            }).ToList();

            return cont;
        }

        public RelatorioVM BuscarRelatorioContrato(int? nrocontr = null, 
                                                    string situacao = null, 
                                                    string datainicial = null, 
                                                    string datafinal = null,
                                                    int pag = 0)
        {
            bool cancel = situacao != null && situacao.ToUpper() == "CANCELADO";
            bool pendente = situacao != null && situacao.ToUpper() == "PENDENTE";

            DateTime? dtde = null;
            DateTime? dtate = null;

            if (!string.IsNullOrEmpty(datainicial))
            {
                dtde = DateTime.Parse(datainicial);
            }

            if (!string.IsNullOrEmpty(datafinal))
            {
                dtate = DateTime.Parse(datafinal);
            }

            var qres = banco.Contratoes.Where(x => x.Ativo &&
                (situacao == null || situacao == "" || cancel && x.Cancelado || pendente && x.Grupo == null) &&
                (dtde == null || x.DtAlocacao >= dtde)  && (dtate == null || x.DtAlocacao <= dtate) &&
                (nrocontr == 0 || nrocontr == null || x.NumContrato == nrocontr)
            ).Select(c => new ItemRelatorio() {
                DataVenda = c.DtInclusao,
                Situacao = c.Cancelado ? "Cancelado" : c.Grupo == null ? "Pendente" : "Alocado",
                TipoVenda = c.TipoVenda,
                NumContrato = c.NumContrato.ToString(),
                Parcela1 = c.PagtoContratoes.Where(o => o.Parcela == 1).Select(o => o.Pagamento).FirstOrDefault(),
                Parcela2 = c.PagtoContratoes.Where(o => o.Parcela == 2).Select(o => o.Pagamento).FirstOrDefault(),
                Parcela3 = c.PagtoContratoes.Where(o => o.Parcela == 3).Select(o => o.Pagamento).FirstOrDefault(),
                Parcela4 = c.PagtoContratoes.Where(o => o.Parcela == 4).Select(o => o.Pagamento).FirstOrDefault()
            }).ToList();

            return new RelatorioVM() { Itens = ((qres.Count() > 15 && pag > 0) ? qres.Skip((pag - 1) * 15).Take(15).ToList() : qres.Take(15).ToList()), Qtde = qres.Count() };
        }

        public string AlocadoContratoBD(int id, bool alocado)
        {

            var change = banco.Contratoes.Where(x => x.Id == id).FirstOrDefault();

            try
            {
                change.Alocado = alocado;
                banco.SaveChanges();

                return "1";
            }
            catch 
            {
                return "0";
            }

        }
        
    }
}