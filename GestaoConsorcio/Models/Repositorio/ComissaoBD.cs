﻿using GestaoConsorcio.Models;
using GestaoConsorcio.Models.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using GestaoConsorcio.Models.Banco;

namespace GestaoConsorcio.Models.Repositorio
{
    public class ComissaoBD
    {

        private db_gestaocotamelhor_desenvEntities banco = new db_gestaocotamelhor_desenvEntities();


        public string SalvarComissao (ComissaoVM model)
        {
            try
            {

                var busc = banco.Comissaos.Where(x => x.ConsultorId == model.ComboConsultor).FirstOrDefault();

                if (busc == null)
                {
                    Comissao com = new Comissao();

                    com.ConsultorId = model.ComboConsultor;
                    com.Valor = model.Valor;

                    banco.Comissaos.Add(com);
                    banco.SaveChanges();

                    return "1";
                }
                else
                {
                    return "0";
                }

            }
            catch
            {
                return "-1";
            }
        }

        public ComissaoVM Consultores()
        {
            var busca = banco.Usuarios.Where(x => x.Consultor == true).Select(c => new ConsultorVM { 
            
                    consultorid = c.Id,
                    nome = c.Nome,
                    sobrenome = c.Sobrenome
            
            }).ToList();

            ComissaoVM cm = new ComissaoVM();
            cm.consultor = busca;
            return cm;

        } 
        
        
    }
}