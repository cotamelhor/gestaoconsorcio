﻿using GestaoConsorcio.Models;
using GestaoConsorcio.Models.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using GestaoConsorcio.Models.Banco;

namespace GestaoConsorcio.Models.Repositorio
{
    public class BackOfficeBD
    {

        private db_gestaocotamelhor_desenvEntities banco = new db_gestaocotamelhor_desenvEntities();


        public List<DadosAgen>SolicitacaoCont()
        {
            var busca = banco.Agendamentoes.Join(banco.Usuarios,
                ag => ag.ConsultorId,
                usu => usu.Id,(ag,usu) => new {ag,usu}).Where(x=> x.ag.Tipo == "S").OrderBy(x=> x.ag.DtAtendimento).Select(y=> new DadosAgen {
                
                    fg_urgente = y.ag.fg_Urgente,
                    agendamentoid = y.ag.AgendamentoId,
                    dtatendimento = y.ag.DtAtendimento,
                    consultornome = y.usu.Nome,
                    protocolo = y.ag.Protocolo

                }).ToList();

            return busca;
        }

       public List<DadosAgen> TarefaHojeDoc(string id)
        {
            string dtnow = DateTime.Now.Year + "-" + (DateTime.Now.Month < 10 ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString()) + "-" + (DateTime.Now.Day < 10 ? "0" + DateTime.Now.Day.ToString() : DateTime.Now.Day.ToString());
            List<DadosAgen> tarefa = null;
            DateTime dnow = DateTime.Parse(dtnow);
            
            tarefa = banco.Contatos.Join(banco.Agendamentoes,
                                    a => a.AgendamentoID,
                                    b => b.AgendamentoId,
                                    (a, b) => new { a, b }).Join(banco.Usuarios, c => c.b.ConsultorId, d => d.Id, (c, d) => new { c, d })
                                    .Where(a => (a.c.a.DtContato <= dnow) &&
                                                (a.c.b.UltimoContato == a.c.a.ContatoID || a.c.b.UltimoContato == null) &&
                                                a.c.b.fg_Ativo == "1" &&
                                                a.c.b.fg_Finalizado != true &&
                                                a.c.b.NumContrato == null &&
                                                a.c.b.Tipo == "C").OrderBy(c => c.c.a.DtContato).ThenBy(c => c.c.a.Hora).Select(s => new DadosAgen
                                                {

                                                    agendamentoid = s.c.b.AgendamentoId,
                                                    consultornome = s.d.Nome + " " + s.d.Sobrenome,
                                                    hora = s.c.a.Hora == null ? "" : s.c.a.Hora,
                                                    nome = s.c.b.Nome.ToUpper(),
                                                    dtcontato = s.c.a.DtContato.ToString(),
                                                    fone1 = s.c.b.Fone1 == null ? "" : s.c.b.Fone1,
                                                    fone2 = s.c.b.Fone2 == null ? "" : s.c.b.Fone2,
                                                    tipocliente = (s.c.b.Tipo == "C" ? "Venda" : "Ligação")

                                                }).ToList();
            
            return tarefa;
        }

       public List<DadosAgen> TarefaHojeLig(string id)
       {
           string dtnow = DateTime.Now.Year + "-" + (DateTime.Now.Month < 10 ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString()) + "-" + (DateTime.Now.Day < 10 ? "0" + DateTime.Now.Day.ToString() : DateTime.Now.Day.ToString());
           List<DadosAgen> tarefa = null;
           DateTime dnow = DateTime.Parse(dtnow);

               tarefa = banco.Contatos.Join(banco.Agendamentoes,
                                   a => a.AgendamentoID,
                                   b => b.AgendamentoId,
                                   (a, b) => new { a, b }).Join(banco.Usuarios, c => c.b.ConsultorId, d => d.Id, (c, d) => new { c, d })
                                   .Where(a => (a.c.a.DtContato <= dnow) &&
                                               (a.c.b.UltimoContato == a.c.a.ContatoID || a.c.b.UltimoContato == null) &&
                                               a.c.b.fg_Ativo == "1" &&
                                               a.c.b.fg_Finalizado != true &&
                                               a.c.b.NumContrato == null &&
                                                a.c.b.Tipo == "L").OrderBy(c => c.c.a.DtContato).ThenBy(c => c.c.a.Hora).Select(s => new DadosAgen
                                               {
                                                   fg_urgente = s.c.b.fg_Urgente,
                                                   agendamentoid = s.c.b.AgendamentoId,
                                                   consultornome = s.d.Nome + " " + s.d.Sobrenome,
                                                   hora = s.c.a.Hora == null ? "" : s.c.a.Hora,
                                                   nome = s.c.b.Nome.ToUpper(),
                                                   dtcontato = s.c.a.DtContato.ToString(),
                                                   fone1 = s.c.b.Fone1 == null ? "" : s.c.b.Fone1,
                                                   fone2 = s.c.b.Fone2 == null ? "" : s.c.b.Fone2,
                                                   tipocliente = (s.c.b.Tipo == "C" ? "Venda" : "Ligação")

                                               }).ToList();
          
           return tarefa;
       }
        
    }
}