﻿using GestaoConsorcio.Models;
using GestaoConsorcio.Models.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using GestaoConsorcio.Models.Banco;

namespace GestaoConsorcio.Models.Repositorio
{
    public class ComboBD
    {

        private db_gestaocotamelhor_desenvEntities banco = new db_gestaocotamelhor_desenvEntities();


        public List<EstadoCivilVM> ComboEstadoCivilBD()
        {
            var lista = banco.EstadoCivils.Select(y => new EstadoCivilVM
            {

                descricao = y.Descricao,
                estadocivilid = y.EstadoCivilId
            }).ToList();

            return lista;
            
        }

        public List<TipoConsorcioVM> ComboTipoConsorcio()
        {

            var lista = banco.Tipoconsorcios.Select(x => new TipoConsorcioVM{
            
                    tipoconsorcioid = x.TipoConsorcioId,
                    descricao = x.Descricao
            
            }).ToList();

            return lista;
        }

        public List<ConsultorVM> ComboConsultorBD()
        {

            var lista = banco.Usuarios.Where(a=> a.Consultor == true).Select(x => new ConsultorVM
            {
            
                    consultorid = x.Id,
                    nome = x.Nome,
                    sobrenome = x.Sobrenome
            
            }).ToList();

            return lista;
        }

        
        
    }
}