﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using GestaoConsorcio.Models;

namespace GestaoConsorcio.Models.Banco
{
    public partial class Usuario : IUser<int>
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<Usuario, int> manager, string authenticationType)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);

            GenerateClaims(ref userIdentity);

            return userIdentity;
        }

        public ClaimsIdentity GenerateUserIdentity(UserManager<Usuario, int> manager, string authenticationType)
        {
            var userIdentity = manager.CreateIdentity(this, authenticationType);

            GenerateClaims(ref userIdentity);

            return userIdentity;
        }

        public void GenerateClaims(ref ClaimsIdentity userIdentity)
        {
            userIdentity.AddClaim(new Claim(ClaimTypes.GivenName, string.IsNullOrWhiteSpace(Nome) ? "" : Nome));
            userIdentity.AddClaim(new Claim(ClaimTypes.Surname, string.IsNullOrWhiteSpace(Sobrenome) ? "" : Sobrenome));
        }

        public string NomeCompleto()
        {
            var nome = "";
            var sobrenome = "";

            if (!string.IsNullOrWhiteSpace(Nome))
                nome = Nome;

            if (!string.IsNullOrWhiteSpace(Sobrenome))
                sobrenome = Sobrenome;


            return string.Concat(nome, " ", sobrenome);
        }
        /*
        public bool PodeResetarSenha()
        {
            if (!UltimoResetSenha.HasValue)
                return true;

            return DateTime.Now.Subtract(UltimoResetSenha.Value).TotalHours >= 24;
        }*/
    }
}
