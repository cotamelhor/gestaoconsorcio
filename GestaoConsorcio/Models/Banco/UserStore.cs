﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using GestaoConsorcio.Models.Banco;

namespace GestaoConsorcio.Models.Banco
{
    public partial class UserStore :
        IQueryableUserStore<Usuario, int>, IUserPasswordStore<Usuario, int>, IUserLoginStore<Usuario, int>,
        IUserClaimStore<Usuario, int>, IUserRoleStore<Usuario, int>, IUserSecurityStampStore<Usuario, int>,
        IUserEmailStore<Usuario, int>, IUserPhoneNumberStore<Usuario, int>, IUserTwoFactorStore<Usuario, int>,
        IUserLockoutStore<Usuario, int>
    {
        private readonly db_gestaocotamelhor_desenvEntities db;

        public UserStore(db_gestaocotamelhor_desenvEntities db)
        {
            if (db == null)
            {
                throw new ArgumentNullException("db");
            }

            this.db = db;
        }

        public IQueryable<Usuario> Users
        {
            get { return this.db.Usuarios; }
        }

        public Task CreateAsync(Usuario user)
        {
           
            this.db.Usuarios.Add(user);
            return this.db.SaveChangesAsync();
        }

       

        public Task DeleteAsync(Usuario user)
        {
            this.db.Usuarios.Remove(user);
            return this.db.SaveChangesAsync();
        }

        public Task<Usuario> FindByIdAsync(int userId)
        {
            return this.db.Usuarios
                .Include(u => u.UsuarioLogins).Include(u => u.Grupoes).Include(u => u.UsuarioClaims)
                .FirstOrDefaultAsync(u => u.Id.Equals(userId));
        }

        public Task<Usuario> FindByNameAsync(string userName)
        {
            return this.db.Usuarios
                .Include(u => u.UsuarioLogins).Include(u => u.Grupoes).Include(u => u.UsuarioClaims)
                .FirstOrDefaultAsync(u => u.UserName == userName);
        }

        public Task UpdateAsync(Usuario user)
        {
            this.db.Entry<Usuario>(user).State = EntityState.Modified;
            return this.db.SaveChangesAsync();
        } 

        public Task<string> GetPasswordHashAsync(Usuario user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return Task.FromResult(user.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(Usuario user)
        {
            return Task.FromResult(user.PasswordHash != null);
        }

        public Task SetPasswordHashAsync(Usuario user, string passwordHash)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            user.PasswordHash = passwordHash;
            return Task.FromResult(0);
        } 

        public Task AddLoginAsync(Usuario user, UserLoginInfo login)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (login == null)
            {
                throw new ArgumentNullException("login");
            }

            var userLogin = Activator.CreateInstance<UsuarioLogin>();
            userLogin.UserID = user.Id;
            userLogin.LoginProvider = login.ProviderKey;
            userLogin.ProviderKey = login.ProviderKey;
            user.UsuarioLogins.Add(userLogin);
            return Task.FromResult(0);
        }

        public async Task<Usuario> FindAsync(UserLoginInfo login)
        {
            if (login == null)
            {
                throw new ArgumentNullException("login");
            }

            var provider = login.LoginProvider;
            var key = login.ProviderKey;

            var userLogin = await this.db.UsuarioLogins.FirstOrDefaultAsync(l => l.LoginProvider == provider && l.ProviderKey == key);

            if (userLogin == null)
            {
                return default(Usuario);
            }

            return await this.db.Usuarios
                .Include(u => u.UsuarioLogins).Include(u => u.Grupoes).Include(u => u.UsuarioClaims)
                .FirstOrDefaultAsync(u => u.Id.Equals(userLogin.UserID));
        }

        public Task<IList<UserLoginInfo>> GetLoginsAsync(Usuario user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return Task.FromResult<IList<UserLoginInfo>>(user.UsuarioLogins.Select(l => new UserLoginInfo(l.LoginProvider, l.ProviderKey)).ToList());
        }

        public Task RemoveLoginAsync(Usuario user, UserLoginInfo login)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (login == null)
            {
                throw new ArgumentNullException("login");
            }

            var provider = login.LoginProvider;
            var key = login.ProviderKey;

            var item = user.UsuarioLogins.SingleOrDefault(l => l.LoginProvider == provider && l.ProviderKey == key);

            if (item != null)
            {
                user.UsuarioLogins.Remove(item);
            }

            return Task.FromResult(0);
        } 

        public Task AddClaimAsync(Usuario user, Claim claim)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (claim == null)
            {
                throw new ArgumentNullException("claim");
            }

            var item = Activator.CreateInstance<UsuarioClaim>();
            item.UserID = user.Id;
            item.ClaimType = claim.Type;
            item.ClaimValue = claim.Value;
            user.UsuarioClaims.Add(item);
            return Task.FromResult(0);
        }

        public Task<IList<Claim>> GetClaimsAsync(Usuario user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return Task.FromResult<IList<Claim>>(user.UsuarioClaims.Select(c => new Claim(c.ClaimType, c.ClaimValue)).ToList());
        }

        public Task RemoveClaimAsync(Usuario user, Claim claim)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (claim == null)
            {
                throw new ArgumentNullException("claim");
            }

            foreach (var item in user.UsuarioClaims.Where(uc => uc.ClaimValue == claim.Value && uc.ClaimType == claim.Type).ToList())
            {
                user.UsuarioClaims.Remove(item);
            }

            foreach (var item in this.db.UsuarioClaims.Where(uc => uc.UserID.Equals(user.Id) && uc.ClaimValue == claim.Value && uc.ClaimType == claim.Type).ToList())
            {
                this.db.UsuarioClaims.Remove(item);
            }

            return Task.FromResult(0);
        } 

        public Task AddToRoleAsync(Usuario user, string roleName)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (string.IsNullOrWhiteSpace(roleName))
            {
                throw new ArgumentException("Valor não pode ser nulo", "roleName");
            }

            var userRole = this.db.Grupoes.SingleOrDefault(r => r.Name == roleName);

            if (userRole == null)
            {
                throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, "Grupo não encontrado", new object[] { roleName }));
            }

            user.Grupoes.Add(userRole);
            return Task.FromResult(0);
        }

        public Task<IList<string>> GetRolesAsync(Usuario user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return Task.FromResult<IList<string>>(user.Grupoes.Join(this.db.Grupoes, ur => ur.Id, r => r.Id, (ur, r) => r.Name).ToList());
        }

        public Task<bool> IsInRoleAsync(Usuario user, string roleName)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (string.IsNullOrWhiteSpace(roleName))
            {
                throw new ArgumentException("Valor não pode ser nulo ou vazio", "roleName");
            }

            return
                Task.FromResult<bool>(
                    this.db.Grupoes.Any(r => r.Name == roleName && r.Usuarios.Any(u => u.Id.Equals(user.Id))));
        }

        public Task RemoveFromRoleAsync(Usuario user, string roleName)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (string.IsNullOrWhiteSpace(roleName))
            {
                throw new ArgumentException("Valor não pode ser nulo ou vazio", "roleName");
            }

            var userRole = user.Grupoes.SingleOrDefault(r => r.Name == roleName);

            if (userRole != null)
            {
                user.Grupoes.Remove(userRole);
            }

            return Task.FromResult(0);
        } 

        public Task<string> GetSecurityStampAsync(Usuario user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return Task.FromResult(user.SecurityStamp);
        }

        public Task SetSecurityStampAsync(Usuario user, string stamp)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            user.SecurityStamp = stamp;
            return Task.FromResult(0);
        } 

        public Task<Usuario> FindByEmailAsync(string email)
        {
            return this.db.Usuarios
                .Include(u => u.UsuarioLogins).Include(u => u.Grupoes).Include(u => u.UsuarioClaims)
                .FirstOrDefaultAsync(u => u.Email == email);
        }

        public Task<string> GetEmailAsync(Usuario user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return Task.FromResult(user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(Usuario user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return Task.FromResult(user.EmailConfirmed);
        }

        public Task SetEmailAsync(Usuario user, string email)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            user.Email = email;
            return Task.FromResult(0);
        }

        public Task SetEmailConfirmedAsync(Usuario user, bool confirmed)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            user.EmailConfirmed = confirmed;
            return Task.FromResult(0);
        }

        public Task<string> GetPhoneNumberAsync(Usuario user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return Task.FromResult(user.PhoneNumber);
        }

        public Task<bool> GetPhoneNumberConfirmedAsync(Usuario user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return Task.FromResult(user.PhoneNumberConfirmed);
        }

        public Task SetPhoneNumberAsync(Usuario user, string phoneNumber)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            user.PhoneNumber = phoneNumber;
            return Task.FromResult(0);
        }

        public Task SetPhoneNumberConfirmedAsync(Usuario user, bool confirmed)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            user.PhoneNumberConfirmed = confirmed;
            return Task.FromResult(0);
        }

        public Task<bool> GetTwoFactorEnabledAsync(Usuario user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return Task.FromResult(user.TwoFactorEnabled);
        }

        public Task SetTwoFactorEnabledAsync(Usuario user, bool enabled)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            user.TwoFactorEnabled = enabled;
            return Task.FromResult(0);
        }

        public Task<int> GetAccessFailedCountAsync(Usuario user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return Task.FromResult(user.AccessFailedCount);
        }

        public Task<bool> GetLockoutEnabledAsync(Usuario user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return Task.FromResult(user.LockoutEnabled);
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(Usuario user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return Task.FromResult(
                user.LockoutEndDateUtc.HasValue ?
                    new DateTimeOffset(DateTime.SpecifyKind(user.LockoutEndDateUtc.Value, DateTimeKind.Utc)) :
                    new DateTimeOffset());
        }

        public Task<int> IncrementAccessFailedCountAsync(Usuario user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            user.AccessFailedCount++;
            return Task.FromResult(user.AccessFailedCount);
        }

        public Task ResetAccessFailedCountAsync(Usuario user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            user.AccessFailedCount = 0;
            return Task.FromResult(0);
        }

        public Task SetLockoutEnabledAsync(Usuario user, bool enabled)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            user.LockoutEnabled = enabled;
            return Task.FromResult(0);
        }

        public Task SetLockoutEndDateAsync(Usuario user, DateTimeOffset lockoutEnd)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            user.LockoutEndDateUtc = lockoutEnd == DateTimeOffset.MinValue ? null : new DateTime?(lockoutEnd.UtcDateTime);
            return Task.FromResult(0);
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing && this.db != null)
            {
                this.db.Dispose();
            }
        }
    }
}
