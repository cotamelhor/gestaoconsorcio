//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GestaoConsorcio.Models.Banco
{
    using System;
    using System.Collections.Generic;
    
    public partial class ObservacaoContato
    {
        public ObservacaoContato()
        {
            this.ObsTags = new HashSet<ObsTag>();
        }
    
        public int ObservacaoContatoID { get; set; }
        public Nullable<int> ContatoCampanhaFK { get; set; }
        public System.DateTime CriadoEm { get; set; }
        public string Texto { get; set; }
        public int UsuarioFK { get; set; }
    
        public virtual ContatoCampanha ContatoCampanha { get; set; }
        public virtual Usuario Usuario { get; set; }
        public virtual ICollection<ObsTag> ObsTags { get; set; }
    }
}
