﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using GestaoConsorcio.Models.Banco;

namespace GestaoConsorcio.Models.Banco
{
    public class RoleStore : IQueryableRoleStore<Grupo, int>
    {
        private readonly db_gestaocotamelhor_desenvEntities db;

        public RoleStore(db_gestaocotamelhor_desenvEntities db)
        {
            this.db = db;
        }

        public IQueryable<Grupo> Roles
        {
            get { return this.db.Grupoes; }
        }
        

        public virtual Task CreateAsync(Grupo role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }

            this.db.Grupoes.Add(role);
            return this.db.SaveChangesAsync();
        }

        public Task DeleteAsync(Grupo role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }

            this.db.Grupoes.Remove(role);
            return this.db.SaveChangesAsync();
        }

        public Task<Grupo> FindByIdAsync(int roleId)
        {
            return this.db.Grupoes.FindAsync(new[] { roleId });
        }

        public Task<Grupo> FindByNameAsync(string roleName)
        {
            return this.db.Grupoes.FirstOrDefaultAsync(r => r.Name == roleName);
        }

        public Task UpdateAsync(Grupo role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }

            this.db.Entry(role).State = EntityState.Modified;
            return this.db.SaveChangesAsync();
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing && this.db != null)
            {
                this.db.Dispose();
            }
        }
    }
}
