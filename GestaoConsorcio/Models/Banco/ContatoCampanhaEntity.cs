﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestaoConsorcio.Models.Banco
{
    public partial class ContatoCampanha
    {
        public string UltimaAcaoDesc {
            get {

                string ret = "";
                List<int> listaInv = new List<int>() { 2, 4, 5, 7, 11 };


                if (this.CCampTags.Any(x=>listaInv.Contains(x.TagContatoFK)))
                {
                    ret = "Inválido";

                }
                else if (this.CCampTags.Any(x => x.TagContatoFK == 6))
                {
                    ret = "Finalizado";
                }
                else if(this.CCampTags.Any(x => x.TagContatoFK == 8))
                {
                    ret = "Proposta";
                }
                else if (this.ObservacaoContatoes.Count() > 0)
                {
                    ret = "Em Andamento";
                }

                return ret;
            }
        }
    }
}