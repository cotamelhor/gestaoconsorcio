//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GestaoConsorcio.Models.Banco
{
    using System;
    using System.Collections.Generic;
    
    public partial class LogSm
    {
        public int LogSmsID { get; set; }
        public System.DateTime DtEnvio { get; set; }
        public string Msg { get; set; }
        public string Telefone { get; set; }
        public int UsuarioFK { get; set; }
        public Nullable<int> Ligacao { get; set; }
    }
}
