﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestaoConsorcio.Models.Cache
{
    public class MelhoresPropostasCache
    {
        public MelhoresPropostasCache(int valorCarta, int parcela, int lance)
        {
            ValorCarta = valorCarta;
            Parcela = parcela;
            Lance = lance;
        }

        public int ValorCarta { get; set; }

        public int Parcela { get; set; }

        public int Lance { get; set; }
    }
}