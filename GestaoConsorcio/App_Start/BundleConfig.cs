﻿using System.Web.Optimization;

namespace GestaoConsorcio
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            #region Css

            bundles.Add(new StyleBundle("~/bundles/styles")
                .Include("~/Content/bootstrap.min.css", new CssRewriteUrlTransform())
                .Include("~/fonts/font-awesome/css/font-awesome.min.css", new CssRewriteUrlTransform())
                .Include("~/Content/sb-admin.css", new CssRewriteUrlTransform())
                .Include("~/Content/maxlength.css", new CssRewriteUrlTransform())
                .Include("~/Content/plugins/morris.css", new CssRewriteUrlTransform())
                .Include("~/Content/Site.css", new CssRewriteUrlTransform())
                );

            #endregion

            #region Js

            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
            "~/Scripts/jquery-1.10.2.js",
            "~/Scripts/bootstrap.min.js",
            "~/Scripts/jquery.bootpag.js",
            "~/Scripts/maxlength.js",
            "~/Scripts/jquery.mask.js",
            //"~/Scripts/plugins/morris/morris.min.js",
            //"~/Scripts/plugins/morris/raphael.min.js",
            //"~/Scripts/plugins/morris/morris-data.js",
            "~/Scripts/layout.js"));

            #endregion

        }
    }
}
