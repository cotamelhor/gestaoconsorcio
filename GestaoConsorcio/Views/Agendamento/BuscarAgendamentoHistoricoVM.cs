﻿using System;

namespace GestaoConsorcio.Views.Agendamento
{
    public class BuscarAgendamentoHistoricoVM
    {
        public int ConsultorId { get; set; }
        public string Cliente { get; set; }
        public string Tipo { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
    }
}