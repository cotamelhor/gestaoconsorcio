﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace ConsultaChatLib
{
    public class HTMLParser
    {
        public static string SubstituiConteudoParm(string parms, string campo, string valor)
        {
            Regex padrao = new Regex(string.Format("{0}=[^&]*&", campo));
            string texto = padrao.Replace(parms, string.Format("{0}={1}&", campo, valor));
            return texto;
        }

        public static Chat BuscarLinhasChat(string texto)
        {
            try
            {
                List<Detalhe> conversa = new List<Detalhe>();
                HtmlDocument doc = new HtmlDocument();
                
                doc.LoadHtml(texto);


                Chat retorno = new Chat();

                retorno.Cliente = doc.DocumentNode.SelectSingleNode("//span[@id='lbl_Apelido']/../../../../td[2]/font/b").InnerText;
                retorno.Sessao = doc.DocumentNode.SelectSingleNode("//span[@id='lbl_Sessao']/../../../../td[2]/font/b").InnerText;
                retorno.Resumo = doc.DocumentNode.SelectSingleNode("//font[@class='textop']").InnerText;
                retorno.Duracao = doc.DocumentNode.SelectSingleNode("//span[@id='lbl_Duracao']/../../../../td[4]/font/b").InnerText;
                retorno.IP = doc.DocumentNode.SelectSingleNode("//span[@id='lbl_IP']/../../../../td[2]/font/b").InnerText; 
                
                
                
                var dlg = doc.DocumentNode.SelectNodes("//table[@id='Table5']//tr");


                foreach (HtmlNode node in dlg)
                {
                    if (node.InnerText != null && !node.InnerText.Trim().Equals(""))
                    {
                        if (node.InnerText.Trim() != "Resumo" && node.InnerText.Trim() != "Diálogo" && node.InnerText.Trim() != "&nbsp;")
                        {

                            Detalhe del = new Detalhe();

                            del.Hora = node.InnerText.Trim().Substring(1, 8);
                            var clean = node.InnerText.Trim().Replace("&nbsp;", "").Replace("(" + del.Hora + ") -", "");
                            var at = clean.IndexOf(':').ToString();
                            del.Nome = clean.Substring(0,(int.Parse(at)));
                            del.Msg = clean.Replace(del.Nome+":","");

                            conversa.Add(del);
                        }
                    }
                }

                retorno.Conversa = conversa;

                return retorno;
            }
            catch 
            {
                return null;
            }
        }
    }
}
