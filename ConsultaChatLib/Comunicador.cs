﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace ConsultaChatLib
{
    public class Comunicador
    {
        public const string TEXTO_URL = "Disparando para {0}";
        public const int TIMEOUT = 120000;
        public const string MPOST = "POST";
        public const string MGET = "GET";
        private CookieContainer cookies = new CookieContainer();
        public Encoding encoding = Encoding.UTF8;

        public string URLPost(string endereco, string postData)
        {
            String result = "";
            for (int rep = 0; rep < 10; rep++)
            {
                try
                {
                    Console.WriteLine(TEXTO_URL, endereco);
                    Uri url = new Uri(endereco);
                    HttpWebRequest http = (HttpWebRequest)HttpWebRequest.Create(url);
                    http.CookieContainer = cookies;
                    http.Timeout = TIMEOUT;
                    http.ContentType = "application/x-www-form-urlencoded";
                    http.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36";
                    http.Method = MPOST;
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    http.ContentLength = byteArray.Length;
                    Stream ostream = http.GetRequestStream();
                    ostream.Write(byteArray, 0, byteArray.Length);

                    ostream.Close();

                    HttpWebResponse response = (HttpWebResponse)http.GetResponse();

                    Stream istream = response.GetResponseStream();
                    encoding = Encoding.GetEncoding(response.CharacterSet);
                    StreamReader stream = new StreamReader(response.GetResponseStream(), encoding);

                    result = stream.ReadToEnd();
                    response.Close();
                    stream.Close();

                    break;
                }
                catch (Exception e)
                {
                    Console.Write(e);
                }
            }
            return result;
        }

        public string URLGet(string txturl)
        {
            String result = "";
            for (int rep = 0; rep < 10; rep++)
            {
                try
                {

                    Uri url = new Uri(txturl);
                    HttpWebRequest http = (HttpWebRequest)HttpWebRequest.Create(url);
                    http.CookieContainer = cookies;
                    http.Timeout = TIMEOUT;
                    http.ContentType = "application/x-www-form-urlencoded";
                    http.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36";
                    http.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
                    http.Headers.Add("Accept-Encoding", "gzip, deflate, sdch");
                    http.Headers.Add("Accept-Language", "pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4");
                    http.Method = MGET;
                    HttpWebResponse response = (HttpWebResponse)http.GetResponse();
                    encoding = Encoding.GetEncoding(response.CharacterSet);
                    StreamReader stream = new StreamReader(response.GetResponseStream(), encoding);
                    result = stream.ReadToEnd();
                    response.Close();
                    stream.Close();

                    break;
                }
                catch (Exception e)
                {
                    Console.Write(e);
                }
            }
            return result;
        }


        public string URLGet_WithNewCookie(string txturl)
        {
            String result = "";
            for (int rep = 0; rep < 10; rep++)
            {
                try
                {
                    CookieContainer cks = this.CopyContainer(cookies);

                    Uri url = new Uri(txturl);
                    HttpWebRequest http = (HttpWebRequest)HttpWebRequest.Create(url);
                    http.CookieContainer = cks;
                    http.Timeout = 30000;
                    http.ContentType = "application/x-www-form-urlencoded";
                    http.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36";
                    http.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
                    http.Headers.Add("Accept-Encoding", "gzip, deflate, sdch");
                    http.Headers.Add("Accept-Language", "pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4");
                    http.Method = MGET;
                    HttpWebResponse response = (HttpWebResponse)http.GetResponse();
                    encoding = Encoding.GetEncoding(response.CharacterSet);
                    StreamReader stream = new StreamReader(response.GetResponseStream(), encoding);
                    result = stream.ReadToEnd();
                    response.Close();
                    stream.Close();

                    break;
                }
                catch (Exception e)
                {
                    Console.Write(e);
                }
            }
            return result;
        }

        private CookieContainer CopyContainer(CookieContainer container)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, container);
                stream.Seek(0, SeekOrigin.Begin);
                return (CookieContainer)formatter.Deserialize(stream);
            }
        }

    }
}
