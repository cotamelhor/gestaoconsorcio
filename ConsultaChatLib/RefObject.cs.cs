﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ConsultaChatLib
{
    public class RefObject
    {
        private object obj = null;
        public RefObject(object _obj)
        {
            this.obj = _obj;
        }

        public List<PropertyInfo> Propriedades()
        {
            return new List<PropertyInfo>(this.obj.GetType().GetProperties());
        }

        public object ValorPropriedade(PropertyInfo prop)
        {
            return prop.GetValue(this.obj, null);
        }

        public void Escrever(PropertyInfo prop, object valor)
        {
            prop.SetValue(this.obj, valor, null);
        }

    }
}
