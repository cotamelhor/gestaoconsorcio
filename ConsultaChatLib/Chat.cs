﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsultaChatLib
{
    public class Chat
    {
        public string Protocolo { get; set; }
        public string Cliente { get; set; }
        public string Sessao { get; set; }
        public string Resumo { get; set; }
        public string Duracao { get; set; }
        public string IP { get; set; }
        public List<Detalhe> Conversa { get; set; }
    }

    public class Detalhe
    {
        public string Hora { get; set; }
        public string Nome { get; set; }
        public string Msg { get; set; }

    }
}
